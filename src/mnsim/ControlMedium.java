/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;

import java.util.ArrayList;
import java.util.Map;
import mnsim.mac.MAC;
import mnsim.ra.RA;
import org.w3c.dom.Element;

/**
 *
 * @author lucas
 */
public class ControlMedium {
    
    private RA ra;
    private MAC mac;
    private Topology top;
    
    private Map<Packet, Path> mappedPacks; // Packets that have been accepted into the network
    private Map<Long, Packet> activePacks; // Packets that have been accepted or that are waiting for a decision 
    
    private Event currentEvent;
    private EventScheduler events;
    private Tracer tr = Tracer.getTracerObject();
    private MyStatistics st = MyStatistics.getMyStatisticsObject();

    ControlMedium(Element xmlRA, Element xmlMAC, Topology top, EventScheduler events) {
        Class RAClass, MACClass;
        try {
            String raModule = "mnsim.ra." + xmlRA.getAttribute("module");
            String macModule = "mnsim.mac." + xmlMAC.getAttribute("module");
            RAClass = Class.forName(raModule);
            MACClass = Class.forName(macModule);
            this.ra = (RA) RAClass.newInstance();
            this.ra.simulationInterface(this);
            this.mac = (MAC) MACClass.newInstance();
            this.mac.simulationInterface(top, events);
        } catch (Throwable t) {
            t.printStackTrace();
        }
        
        this.top = top;
        this.events = events;
    }
    
    public void newEvent(Event event) {
        this.currentEvent = event;
        if(event instanceof PacketArrivalEvent){
            newPacket(((PacketArrivalEvent) event).getPack());
        } else {
            ra.packetDeparture(((PacketDepartureEvent) event).getId());
            removePacket(((PacketDepartureEvent) event).getId());
        }
    }
    
    public boolean acceptPacket(long id, int[] route) {
        //TODO
        return true;
    }
    
    public boolean blockPacket(long id) {
        //TODO
        return true;
    }
    
    public boolean checkMyMedium(int node){
        //TODO
        return true;
    }

    private void newPacket(Packet pack) {
        if(!activePacks.containsKey(pack.getId())){
            activePacks.put(pack.getId(), pack);
            ra.packetArrival(pack);
        } else {
            Event[] sameTimeEvents = getSameTimeEvents(currentEvent.getTime());
            if(!hasColision(sameTimeEvents)){
                mac.sendingAction(pack, mappedPacks.get(pack), currentEvent.getTime());
            } else {
                mac.collisionAction(colisionNodes(sameTimeEvents));
            }
        }
    }

    private void removePacket(long id) {
        Packet pack;
        Path path;

        if (activePacks.containsKey(id)) {
            pack = activePacks.get(id);
            if (mappedPacks.containsKey(pack)) {
                path = mappedPacks.get(pack);
                mac.departurePacket(pack, path);
                mappedPacks.remove(pack);
            }
            activePacks.remove(id);
        }
    }
    
    /**
     * Retrieves all events in this time
     * @param time the time event
     * @return all events in this time
     */
    private Event[] getSameTimeEvents(double time){
        ArrayList<Event> sameEvents = new ArrayList<>();
        Event auxevent;
        while (events.numEvents() > 0) {            
            auxevent = events.popEvent();
            if(auxevent.getTime() == time){
                sameEvents.add(auxevent);
            } else {
                events.addEvent(auxevent);
                if(auxevent.getTime() > time){
                    break;
                }
            }
        }
        Event[] sameEventsVector = new Event[sameEvents.size()];
        for (int i = 0; i < sameEvents.size(); i++) {
            sameEventsVector[i] = sameEvents.get(i);
        }
        return sameEventsVector;
    }

    private boolean hasColision(Event[] sameTimeEvents) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private int[] colisionNodes(Event[] sameTimeEvents) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
