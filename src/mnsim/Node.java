/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;

/**
 *
 * @author lucas
 */
public class Node {
    
    private final int id;
    private final int radius;
    private final boolean[] channels;
    private int state;

    public Node(int id, int radius, int channels) {
        this.id = id;
        this.radius = radius;
        this.channels = new boolean[channels];
        for (int i = 0; i < channels; i++) {
            this.channels[i] = false;
        }
        this.state = Defines.IdleState;
    }
    
    public int getNodeState(){
        return state;
    }
    
    public void setSendingState(){
        state = Defines.SendingState;
    }
    
    public void setListeningState(){
        state = Defines.ListeningState;
    }
    
    public void setIdleState(){
        state = Defines.IdleState;
    }
    
    public void setNAVState(){
        state = Defines.NAVState;
    }

    public int getId() {
        return id;
    }

    public int getRadius() {
        return radius;
    }

    public int getNumberChannels() {
        return channels.length;
    }
    
    public boolean getStateChannel(int ch) {
        return channels[ch];
    }
    
    public void freeChanel(int ch){
        channels[ch] = false;
    }
    
    public void reserveChanel(int ch){
        channels[ch] = true;
    }
}
