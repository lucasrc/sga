/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;

import java.util.Arrays;
import java.util.Scanner;
import mnsim.tc.RNG;
import mnsim.util.Distribution;
import mnsim.tools.DrawGraph;
import mnsim.util.Graph;
import javax.swing.SwingUtilities;
import mnsim.tc.BIP;
import mnsim.tc.BLU;
import mnsim.tc.GG;
import mnsim.tc.KNNG;
import mnsim.tc.MST;
import mnsim.tc.NNG;
import mnsim.tc.UDG;
import mnsim.tc.YG;
import mnsim.util.Dijkstra;

/**
 *
 * @author lucas
 */
public class RMSimulatorMain0 {

    public static void main2(String[] args) {
        
        String usage = "Usage: RMSimulator [-a] [number_of_graphs] <uniform, normal, poisson> seed network_space number_of_nodes radius";
        if ((args.length < 5 || args.length > 7) && args.length != 6) {
            System.out.println(usage);
            System.exit(0);
        }
        String type;
        int nGraphs;
        String distribuition;
        int seed;
        int sizeGraph;
        int nodes;
        int radius;
        int kNeighbor = 4;
        int kSector = 4;
        
        if (args.length == 7) {
            type = args[0];
            nGraphs = Integer.parseInt(args[1]);
            distribuition = args[2];
            seed = Integer.parseInt(args[3]);
            sizeGraph = Integer.parseInt(args[4]);
            nodes = Integer.parseInt(args[5]);
            radius = Integer.parseInt(args[6]);
        } else {
            type = "";
            nGraphs = 1;
            distribuition = args[0];
            seed = Integer.parseInt(args[1]);
            sizeGraph = Integer.parseInt(args[2]);
            nodes = Integer.parseInt(args[3]);
            radius = Integer.parseInt(args[4]);
        }
        Graph[] graphs = new Graph[nGraphs];
        Graph g = new Graph(sizeGraph);
        if (type.equals("-a")) {
            long tempoInicio = System.currentTimeMillis();
            if (distribuition.equals("uniform")) {
                System.out.println("Gerando os " + nGraphs + " grafos aleatórios (Distribuição Uniforme)");
                Distribution distX = new Distribution(1, seed);
                Distribution distY = new Distribution(2, seed);
                for (int i = 0; i < graphs.length; i++) {
                    graphs[i] = new Graph(sizeGraph);
                    for (int j = 0; j < nodes; j++) {
                        int x0 = distX.nextInt(sizeGraph);
                        int y0 = distY.nextInt(sizeGraph);
                        while (graphs[i].isVertex(x0, y0)) {
                            x0 = distX.nextInt(sizeGraph);
                            y0 = distY.nextInt(sizeGraph);
                        }
                        graphs[i].addVertex(j, x0, y0);
                    }
                }
            }
            if (distribuition.equals("normal")) {
                System.out.println("Gerando os " + nGraphs + " grafos aleatórios (Distribuição Normal)");
                Distribution distX = new Distribution(1, seed);
                Distribution distY = new Distribution(2, seed);
                for (int i = 0; i < graphs.length; i++) {
                    graphs[i] = new Graph(sizeGraph);
                    for (int j = 0; j < nodes; j++) {
                        int x0 = (int) Math.round(distX.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                        int y0 = (int) Math.round(distY.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                        while (graphs[i].isVertex(x0, y0)) {
                            x0 = (int) Math.round(distX.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                            y0 = (int) Math.round(distY.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                        }
                        graphs[i].addVertex(j, x0, y0);
                    }
                }
            }
            if (distribuition.equals("poisson")) {
                System.out.println("Gerando os " + nGraphs + " grafos aleatórios (Distribuição Poissom)");
                Distribution distX = new Distribution(1, seed);
                Distribution distY = new Distribution(2, seed);
                for (int i = 0; i < graphs.length; i++) {
                    graphs[i] = new Graph(sizeGraph);
                    for (int j = 0; j < nodes; j++) {
                        int x0 = (int) Math.round(distX.nextExponential(sizeGraph, 0, sizeGraph));
                        int y0 = (int) Math.round(distY.nextExponential(sizeGraph, 0, sizeGraph));
                        while (graphs[i].isVertex(x0, y0)) {
                            x0 = (int) Math.round(distX.nextExponential(sizeGraph, 0, sizeGraph));
                            y0 = (int) Math.round(distY.nextExponential(sizeGraph, 0, sizeGraph));
                        }
                        graphs[i].addVertex(j, x0, y0);
                    }
                }
            }
            System.out.println(nGraphs + " grafos aleatórios gerados ["+ (System.currentTimeMillis()-tempoInicio) +"ms]");
            //Creating graphs Topology controls
            System.out.println("Criando Topologias UDG, NNG, RNG, MST, KNNG, YG, GG, BLU, BIP");
            Graph[] udgGraphs = new Graph[graphs.length];
            Graph[] nngGraphs = new Graph[graphs.length];
            Graph[] rngGraphs = new Graph[graphs.length];
            Graph[] mstGraphs = new Graph[graphs.length];
            Graph[] knngGraphs = new Graph[graphs.length];
            Graph[] ygGraphs = new Graph[graphs.length];
            Graph[] ggGraphs = new Graph[graphs.length];
            Graph[] bluGraphs = new Graph[graphs.length];
            Graph[] bipGraphs = new Graph[graphs.length];
            for (int i = 0; i < graphs.length; i++) {
                udgGraphs[i] = UDG.unitDiskGraph(graphs[i], radius);
                nngGraphs[i] = NNG.nearestNeighborGraph(graphs[i], radius);
                rngGraphs[i] = RNG.relativeNeighborhoodGraph(graphs[i], radius);
                mstGraphs[i] = MST.minimumSpanningTree(udgGraphs[i]);
                knngGraphs[i] = KNNG.knearestNeighborGraph(graphs[i], radius, kNeighbor);
                ygGraphs[i] = YG.yaoGraph(graphs[i], radius, kSector);
                ggGraphs[i] = GG.gabrielGraphv(udgGraphs[i]);
                bluGraphs[i] = BLU.broadcastLeastUnicastCost(udgGraphs[i]);
                bipGraphs[i] = BIP.broadcastIncreasePower(graphs[i], radius);
            }
            System.out.println("Topologias criadas ["+ (System.currentTimeMillis()-tempoInicio) +"ms]");
            double minRoute = 0;
            double graphDiameter = 0;
            double maxClusteringCoefficient = 0;
            double sumClusteringCoefficient = 0;
            double averageClusteringCoefficient = 0;
            double globalClusteringCoefficient = 0;
            double averagePathLength = 0;
            double energyCost = 0;
            String s = "";
            System.out.println("Fazendo analise do UDG");
            for (int i = 0; i < graphs.length; i++) {
                minRoute += udgGraphs[i].getMinRoute();
                graphDiameter += udgGraphs[i].getGraphDiameter();
                maxClusteringCoefficient += udgGraphs[i].getMaxClusteringCoefficient();
                sumClusteringCoefficient += udgGraphs[i].getSumClusteringCoefficient();
                averageClusteringCoefficient += udgGraphs[i].getAverageClusteringCoefficient();
                globalClusteringCoefficient += udgGraphs[i].getGlobalClusteringCoefficient();
                averagePathLength += udgGraphs[i].getAveragePathLength();
                energyCost += udgGraphs[i].getCost(radius);
            }
            s = "";
            s += "\nAnalise dos "+ nGraphs +" grafos UDG com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
            s += "\nMenor Rota: \t" + minRoute/(double)nGraphs;
            s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter/(double)nGraphs;
            s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient/(double)nGraphs;
            s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient/(double)nGraphs;
            s += "\nMédia das menores rotas (stretch): \t" + averagePathLength/(double)nGraphs;
            s += "\nCusto de Energia: \t" + energyCost/(double)nGraphs + "\n";
            System.out.println(s);
            System.out.println("Fim da Analise UDG ["+ (System.currentTimeMillis()-tempoInicio) +"ms]");
            
            minRoute = 0;
            graphDiameter = 0;
            maxClusteringCoefficient = 0;
            sumClusteringCoefficient = 0;
            averageClusteringCoefficient = 0;
            globalClusteringCoefficient = 0;
            averagePathLength = 0;
            energyCost = 0;
            s = "";
            System.out.println("Fazendo analise do NNG");
            for (int i = 0; i < graphs.length; i++) {
                minRoute += nngGraphs[i].getMinRoute();
                graphDiameter += nngGraphs[i].getGraphDiameter();
                maxClusteringCoefficient += nngGraphs[i].getMaxClusteringCoefficient();
                sumClusteringCoefficient += nngGraphs[i].getSumClusteringCoefficient();
                averageClusteringCoefficient += nngGraphs[i].getAverageClusteringCoefficient();
                globalClusteringCoefficient += nngGraphs[i].getGlobalClusteringCoefficient();
                averagePathLength += nngGraphs[i].getAveragePathLength();
                energyCost += nngGraphs[i].getCost(radius);
            }
            s = "";
            s += "\nAnalise dos "+ nGraphs +" grafos NNG com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
            s += "\nMenor Rota: \t" + minRoute/(double)nGraphs;
            s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter/(double)nGraphs;
            s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient/(double)nGraphs;
            s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient/(double)nGraphs;
            s += "\nMédia das menores rotas (stretch): \t" + averagePathLength/(double)nGraphs;
            s += "\nCusto de Energia: \t" + energyCost/(double)nGraphs + "\n";
            System.out.println(s);
            System.out.println("Fim da Analise NNG ["+ (System.currentTimeMillis()-tempoInicio) +"ms]");
            
            minRoute = 0;
            graphDiameter = 0;
            maxClusteringCoefficient = 0;
            sumClusteringCoefficient = 0;
            averageClusteringCoefficient = 0;
            globalClusteringCoefficient = 0;
            averagePathLength = 0;
            energyCost = 0;
            s = "";
            System.out.println("Fazendo analise do RNG");
            for (int i = 0; i < graphs.length; i++) {
                minRoute += rngGraphs[i].getMinRoute();
                graphDiameter += rngGraphs[i].getGraphDiameter();
                maxClusteringCoefficient += rngGraphs[i].getMaxClusteringCoefficient();
                sumClusteringCoefficient += rngGraphs[i].getSumClusteringCoefficient();
                averageClusteringCoefficient += rngGraphs[i].getAverageClusteringCoefficient();
                globalClusteringCoefficient += rngGraphs[i].getGlobalClusteringCoefficient();
                averagePathLength += rngGraphs[i].getAveragePathLength();
                energyCost += rngGraphs[i].getCost(radius);
            }
            s = "";
            s += "\nAnalise dos "+ nGraphs +" grafos RNG com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
            s += "\nMenor Rota: \t" + minRoute/(double)nGraphs;
            s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter/(double)nGraphs;
            s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient/(double)nGraphs;
            s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient/(double)nGraphs;
            s += "\nMédia das menores rotas (stretch): \t" + averagePathLength/(double)nGraphs;
            s += "\nCusto de Energia: \t" + energyCost/(double)nGraphs + "\n";
            System.out.println(s);
            System.out.println("Fim da Analise RNG ["+ (System.currentTimeMillis()-tempoInicio) +"ms]");
            
            minRoute = 0;
            graphDiameter = 0;
            maxClusteringCoefficient = 0;
            sumClusteringCoefficient = 0;
            averageClusteringCoefficient = 0;
            globalClusteringCoefficient = 0;
            averagePathLength = 0;
            energyCost = 0;
            s = "";
            System.out.println("Fazendo analise do MST");
            for (int i = 0; i < graphs.length; i++) {
                minRoute += mstGraphs[i].getMinRoute();
                graphDiameter += mstGraphs[i].getGraphDiameter();
                maxClusteringCoefficient += mstGraphs[i].getMaxClusteringCoefficient();
                sumClusteringCoefficient += mstGraphs[i].getSumClusteringCoefficient();
                averageClusteringCoefficient += mstGraphs[i].getAverageClusteringCoefficient();
                globalClusteringCoefficient += mstGraphs[i].getGlobalClusteringCoefficient();
                averagePathLength += mstGraphs[i].getAveragePathLength();
                energyCost += mstGraphs[i].getCost(radius);
            }
            s = "";
            s += "\nAnalise dos "+ nGraphs +" grafos MST com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
            s += "\nMenor Rota: \t" + minRoute/(double)nGraphs;
            s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter/(double)nGraphs;
            s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient/(double)nGraphs;
            s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient/(double)nGraphs;
            s += "\nMédia das menores rotas (stretch): \t" + averagePathLength/(double)nGraphs;
            s += "\nCusto de Energia: \t" + energyCost/(double)nGraphs + "\n";
            System.out.println(s);
            System.out.println("Fim da Analise MST ["+ (System.currentTimeMillis()-tempoInicio) +"ms]");
            
            minRoute = 0;
            graphDiameter = 0;
            maxClusteringCoefficient = 0;
            sumClusteringCoefficient = 0;
            averageClusteringCoefficient = 0;
            globalClusteringCoefficient = 0;
            averagePathLength = 0;
            energyCost = 0;
            s = "";
            System.out.println("Fazendo analise do KNNG");
            for (int i = 0; i < graphs.length; i++) {
                minRoute += knngGraphs[i].getMinRoute();
                graphDiameter += knngGraphs[i].getGraphDiameter();
                maxClusteringCoefficient += knngGraphs[i].getMaxClusteringCoefficient();
                sumClusteringCoefficient += knngGraphs[i].getSumClusteringCoefficient();
                averageClusteringCoefficient += knngGraphs[i].getAverageClusteringCoefficient();
                globalClusteringCoefficient += knngGraphs[i].getGlobalClusteringCoefficient();
                averagePathLength += knngGraphs[i].getAveragePathLength();
                energyCost += knngGraphs[i].getCost(radius);
            }
            s = "";
            s += "\nAnalise dos "+ nGraphs +" grafos KNNG com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
            s += "\nMenor Rota: \t" + minRoute/(double)nGraphs;
            s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter/(double)nGraphs;
            s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient/(double)nGraphs;
            s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient/(double)nGraphs;
            s += "\nMédia das menores rotas (stretch): \t" + averagePathLength/(double)nGraphs;
            s += "\nCusto de Energia: \t" + energyCost/(double)nGraphs + "\n";
            System.out.println(s);
            System.out.println("Fim da Analise KNNG ["+ (System.currentTimeMillis()-tempoInicio) +"ms]");
            
            minRoute = 0;
            graphDiameter = 0;
            maxClusteringCoefficient = 0;
            sumClusteringCoefficient = 0;
            averageClusteringCoefficient = 0;
            globalClusteringCoefficient = 0;
            averagePathLength = 0;
            energyCost = 0;
            s = "";
            System.out.println("Fazendo analise do YG");
            for (int i = 0; i < graphs.length; i++) {
                minRoute += ygGraphs[i].getMinRoute();
                graphDiameter += ygGraphs[i].getGraphDiameter();
                maxClusteringCoefficient += ygGraphs[i].getMaxClusteringCoefficient();
                sumClusteringCoefficient += ygGraphs[i].getSumClusteringCoefficient();
                averageClusteringCoefficient += ygGraphs[i].getAverageClusteringCoefficient();
                globalClusteringCoefficient += ygGraphs[i].getGlobalClusteringCoefficient();
                averagePathLength += ygGraphs[i].getAveragePathLength();
                energyCost += ygGraphs[i].getCost(radius);
            }
            s = "";
            s += "\nAnalise dos "+ nGraphs +" grafos YG com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
            s += "\nMenor Rota: \t" + minRoute/(double)nGraphs;
            s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter/(double)nGraphs;
            s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient/(double)nGraphs;
            s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient/(double)nGraphs;
            s += "\nMédia das menores rotas (stretch): \t" + averagePathLength/(double)nGraphs;
            s += "\nCusto de Energia: \t" + energyCost/(double)nGraphs + "\n";
            System.out.println(s);
            System.out.println("Fim da Analise YG ["+ (System.currentTimeMillis()-tempoInicio) +"ms]");
            
            minRoute = 0;
            graphDiameter = 0;
            maxClusteringCoefficient = 0;
            sumClusteringCoefficient = 0;
            averageClusteringCoefficient = 0;
            globalClusteringCoefficient = 0;
            averagePathLength = 0;
            energyCost = 0;
            s = "";
            System.out.println("Fazendo analise do GG");
            for (int i = 0; i < graphs.length; i++) {
                minRoute += ggGraphs[i].getMinRoute();
                graphDiameter += ggGraphs[i].getGraphDiameter();
                maxClusteringCoefficient += ggGraphs[i].getMaxClusteringCoefficient();
                sumClusteringCoefficient += ggGraphs[i].getSumClusteringCoefficient();
                averageClusteringCoefficient += ggGraphs[i].getAverageClusteringCoefficient();
                globalClusteringCoefficient += ggGraphs[i].getGlobalClusteringCoefficient();
                averagePathLength += ggGraphs[i].getAveragePathLength();
                energyCost += ggGraphs[i].getCost(radius);
            }
            s = "";
            s += "\nAnalise dos "+ nGraphs +" grafos GG com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
            s += "\nMenor Rota: \t" + minRoute/(double)nGraphs;
            s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter/(double)nGraphs;
            s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient/(double)nGraphs;
            s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient/(double)nGraphs;
            s += "\nMédia das menores rotas (stretch): \t" + averagePathLength/(double)nGraphs;
            s += "\nCusto de Energia: \t" + energyCost/(double)nGraphs + "\n";
            System.out.println(s);
            System.out.println("Fim da Analise GG ["+ (System.currentTimeMillis()-tempoInicio) +"ms]");
            
            minRoute = 0;
            graphDiameter = 0;
            maxClusteringCoefficient = 0;
            sumClusteringCoefficient = 0;
            averageClusteringCoefficient = 0;
            globalClusteringCoefficient = 0;
            averagePathLength = 0;
            energyCost = 0;
            s = "";
            System.out.println("Fazendo analise do BLU");
            for (int i = 0; i < graphs.length; i++) {
                minRoute += bluGraphs[i].getMinRoute();
                graphDiameter += bluGraphs[i].getGraphDiameter();
                maxClusteringCoefficient += bluGraphs[i].getMaxClusteringCoefficient();
                sumClusteringCoefficient += bluGraphs[i].getSumClusteringCoefficient();
                averageClusteringCoefficient += bluGraphs[i].getAverageClusteringCoefficient();
                globalClusteringCoefficient += bluGraphs[i].getGlobalClusteringCoefficient();
                averagePathLength += bluGraphs[i].getAveragePathLength();
                energyCost += bluGraphs[i].getCost(radius);
            }
            s = "";
            s += "\nAnalise dos "+ nGraphs +" grafos BLU com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
            s += "\nMenor Rota: \t" + minRoute/(double)nGraphs;
            s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter/(double)nGraphs;
            s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient/(double)nGraphs;
            s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient/(double)nGraphs;
            s += "\nMédia das menores rotas (stretch): \t" + averagePathLength/(double)nGraphs;
            s += "\nCusto de Energia: \t" + energyCost/(double)nGraphs + "\n";
            System.out.println(s);
            System.out.println("Fim da Analise BLU ["+ (System.currentTimeMillis()-tempoInicio) +"ms]");
            
            minRoute = 0;
            graphDiameter = 0;
            maxClusteringCoefficient = 0;
            sumClusteringCoefficient = 0;
            averageClusteringCoefficient = 0;
            globalClusteringCoefficient = 0;
            averagePathLength = 0;
            energyCost = 0;
            s = "";
            System.out.println("Fazendo analise do BIP");
            for (int i = 0; i < graphs.length; i++) {
                minRoute += bipGraphs[i].getMinRoute();
                graphDiameter += bipGraphs[i].getGraphDiameter();
                maxClusteringCoefficient += bipGraphs[i].getMaxClusteringCoefficient();
                sumClusteringCoefficient += bipGraphs[i].getSumClusteringCoefficient();
                averageClusteringCoefficient += bipGraphs[i].getAverageClusteringCoefficient();
                globalClusteringCoefficient += bipGraphs[i].getGlobalClusteringCoefficient();
                averagePathLength += bipGraphs[i].getAveragePathLength();
                energyCost += bipGraphs[i].getCost(radius);
            }
            s = "";
            s += "\nAnalise dos "+ nGraphs +" grafos BIP com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
            s += "\nMenor Rota: \t" + minRoute/(double)nGraphs;
            s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter/(double)nGraphs;
            s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient/(double)nGraphs;
            s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient/(double)nGraphs;
            s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient/(double)nGraphs;
            s += "\nMédia das menores rotas (stretch): \t" + averagePathLength/(double)nGraphs;
            s += "\nCusto de Energia: \t" + energyCost/(double)nGraphs + "\n";
            System.out.println(s);
            System.out.println("Fim da Analise BIP ["+ (System.currentTimeMillis()-tempoInicio) +"ms]");
            //CONTINUAR AKI
            //TODO
            System.out.println("Fim do programa ["+ (System.currentTimeMillis()-tempoInicio) +"ms]");
            
        } else {
            if (distribuition.equals("uniform")) {
                System.out.println("Gerando um grafo aleatório (Distribuição Uniforme)");
                Distribution distX = new Distribution(1, seed);
                Distribution distY = new Distribution(2, seed);
                for (int i = 0; i < nodes; i++) {
                    int x0 = distX.nextInt(sizeGraph);
                    int y0 = distY.nextInt(sizeGraph);
                    while (g.isVertex(x0, y0)) {
                        x0 = distX.nextInt(sizeGraph);
                        y0 = distY.nextInt(sizeGraph);
                    }
                    g.addVertex(i, x0, y0);
                }
            }
            if (distribuition.equals("normal")) {
                System.out.println("Gerando um grafo aleatório (Distribuição Normal)");
                Distribution distX = new Distribution(1, seed);
                Distribution distY = new Distribution(2, seed);
                for (int i = 0; i < nodes; i++) {
                    int x0 = (int) Math.round(distX.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                    int y0 = (int) Math.round(distY.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                    while (g.isVertex(x0, y0)) {
                        x0 = (int) Math.round(distX.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                        y0 = (int) Math.round(distY.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                    }
                    g.addVertex(i, x0, y0);
                }
            }
            if (distribuition.equals("poisson")) {
                System.out.println("Gerando um grafo aleatório (Distribuição Poissom)");
                Distribution distX = new Distribution(1, seed);
                Distribution distY = new Distribution(2, seed);
                for (int i = 0; i < nodes; i++) {
                    int x0 = (int) Math.round(distX.nextExponential(sizeGraph, 0, sizeGraph));
                    int y0 = (int) Math.round(distY.nextExponential(sizeGraph, 0, sizeGraph));
                    while (g.isVertex(x0, y0)) {
                        x0 = (int) Math.round(distX.nextExponential(sizeGraph, 0, sizeGraph));
                        y0 = (int) Math.round(distY.nextExponential(sizeGraph, 0, sizeGraph));
                    }
                    g.addVertex(i, x0, y0);
                }
            }

            //Creating graphs
            Graph udg = UDG.unitDiskGraph(g, radius);
            Graph nng = NNG.nearestNeighborGraph(g, radius);
            Graph rng = RNG.relativeNeighborhoodGraph(g, radius);
            Graph mst = MST.minimumSpanningTree(udg);
            Graph knng = KNNG.knearestNeighborGraph(g, radius, kNeighbor);
            Graph yg = YG.yaoGraph(g, radius, kSector);
            Graph gg = GG.gabrielGraphv(udg);
            Graph blu = BLU.broadcastLeastUnicastCost(udg);
            Graph bip = BIP.broadcastIncreasePower(g, radius);
            
            double minRoute;
            double graphDiameter;
            double maxClusteringCoefficient;
            double sumClusteringCoefficient;
            double averageClusteringCoefficient;
            double globalClusteringCoefficient;
            double averagePathLength;
            double energyCost;
            String s;
            int dijkstraSourceNode;
            int dijkstraDestinationNode;
            int[] dijkstraRoute;
            String dijkstraOption;

            boolean showNumbers = true;

            Scanner sc = new Scanner(System.in);
            
            int option;
            String menu = "";
            
            menu += "Escolha uma opção:\n";
            menu += "\t(1) Plotar grafo (sem arestas)\n";
            menu += "\t(2) Plotar grafo UDG\n";
            menu += "\t(3) Análise grafo UDG\n";
            menu += "\t(4) Usar Dijkstra no grafo UDG\n";
            menu += "\t(5) Plotar grafo NNG\n";
            menu += "\t(6) Análise grafo NNG\n";
            menu += "\t(7) Usar Dijkstra no grafo NNG\n";
            menu += "\t(8) Plotar grafo RNG\n";
            menu += "\t(9) Análise grafo RNG\n";
            menu += "\t(10) Usar Dijkstra no grafo RNG\n";
            menu += "\t(11) Plotar grafo MST\n";
            menu += "\t(12) Análise grafo MST\n";
            menu += "\t(13) Usar Dijkstra no grafo MST\n";
            menu += "\t(14) Plotar grafo KNNG\n";
            menu += "\t(15) Análise grafo KNNG\n";
            menu += "\t(16) Usar Dijkstra no grafo KNNG\n";
            menu += "\t(17) Plotar grafo YG\n";
            menu += "\t(18) Análise grafo YG\n";
            menu += "\t(19) Usar Dijkstra no grafo YG\n";
            menu += "\t(20) Plotar grafo GG\n";
            menu += "\t(21) Análise grafo GG\n";
            menu += "\t(22) Usar Dijkstra no grafo GG\n";
            menu += "\t(23) Plotar grafo BLU\n";
            menu += "\t(24) Análise grafo BLU\n";
            menu += "\t(25) Usar Dijkstra no grafo BLU\n";
            menu += "\t(26) Plotar grafo BIP\n";
            menu += "\t(27) Análise grafo BIP\n";
            menu += "\t(28) Usar Dijkstra no grafo BIP\n";
            menu += "\t(0) Sair do programa\n";
            while (true) {
                System.out.print(menu);
                System.out.print("Opção: ");
                option = sc.nextInt();
                while (option < 0 || option > 28) {
                    System.out.print("Escolha uma opção válida: ");
                    option = sc.nextInt();
                }
                String nameGraph;
                switch (option) {
                    case 1:
                        nameGraph = "Grafo (sem arestas)";
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                DrawGraph.createAndShowGraph(g, nameGraph, showNumbers);
                            }
                        });
                        break;
                    case 2:
                        nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with UDG";
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                DrawGraph.createAndShowGraph(udg, nameGraph, showNumbers);
                            }
                        });
                        break;
                    case 3:
                        //Analise do UDG
                        minRoute = udg.getMinRoute();
                        graphDiameter = udg.getGraphDiameter();
                        maxClusteringCoefficient = udg.getMaxClusteringCoefficient();
                        sumClusteringCoefficient = udg.getSumClusteringCoefficient();
                        averageClusteringCoefficient = udg.getAverageClusteringCoefficient();
                        globalClusteringCoefficient = udg.getGlobalClusteringCoefficient();
                        averagePathLength = udg.getAveragePathLength();
                        energyCost = udg.getCost(radius);
                        s = "";
                        s += "\nAnalise do Grafo UDG com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
                        s += "\nMenor Rota: \t" + minRoute;
                        s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter;
                        s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient;
                        s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient;
                        s += "\nMédia das menores rotas (stretch): \t" + averagePathLength;
                        s += "\nCusto de Energia: \t" + energyCost + "\n";
                        System.out.println(s);
                        break;
                    case 4:
                        //Dijkstra UDG
                        System.out.print("Informa o nó da fonte: ");
                        dijkstraSourceNode = sc.nextInt();
                        System.out.print("Informa o nó de destino: ");
                        dijkstraDestinationNode = sc.nextInt();
                        dijkstraRoute = Dijkstra.getShortestPath(udg, dijkstraSourceNode, dijkstraDestinationNode);
                        if (dijkstraRoute.length > 0) {
                            System.out.println("A menor rota entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + "> é: " + Arrays.toString(dijkstraRoute));
                            System.out.print("Para mostrar a rota no JFrame aparte \"y\": ");
                            dijkstraOption = sc.next();
                            if (dijkstraOption.equals("y")) {
                                nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with UDG";
                                DrawGraph.createAndShowGraphForDijkstra(udg, nameGraph, showNumbers, dijkstraRoute);
                            }
                        } else {
                            System.out.println("Não existem rotas possíveis entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + ">");
                        }
                        break;
                    case 5:
                        nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with NNG";
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                DrawGraph.createAndShowGraph(nng, nameGraph, showNumbers);
                            }
                        });
                        break;
                    case 6:
                        //Analise NNG
                        minRoute = nng.getMinRoute();
                        graphDiameter = nng.getGraphDiameter();
                        maxClusteringCoefficient = nng.getMaxClusteringCoefficient();
                        sumClusteringCoefficient = nng.getSumClusteringCoefficient();
                        averageClusteringCoefficient = nng.getAverageClusteringCoefficient();
                        globalClusteringCoefficient = nng.getGlobalClusteringCoefficient();
                        averagePathLength = nng.getAveragePathLength();
                        energyCost = nng.getCost(radius);
                        s = "";
                        s += "\nAnalise do Grafo NNG com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
                        s += "\nMenor Rota: \t" + minRoute;
                        s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter;
                        s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient;
                        s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient;
                        s += "\nMédia das menores rotas (stretch): \t" + averagePathLength;
                        s += "\nCusto de Energia: \t" + energyCost + "\n";
                        System.out.println(s);
                        break;
                    case 7:
                        //Dijkstra NNG
                        System.out.print("Informa o nó da fonte: ");
                        dijkstraSourceNode = sc.nextInt();
                        System.out.print("Informa o nó de destino: ");
                        dijkstraDestinationNode = sc.nextInt();
                        dijkstraRoute = Dijkstra.getShortestPath(nng, dijkstraSourceNode, dijkstraDestinationNode);
                        if (dijkstraRoute.length > 0) {
                            System.out.println("A menor rota entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + "> é: " + Arrays.toString(dijkstraRoute));
                            System.out.print("Para mostrar a rota no JFrame aparte \"y\": ");
                            dijkstraOption = sc.next();
                            if (dijkstraOption.equals("y")) {
                                nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with NNG";
                                DrawGraph.createAndShowGraphForDijkstra(nng, nameGraph, showNumbers, dijkstraRoute);
                            }
                        } else {
                            System.out.println("Não existem rotas possíveis entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + ">");
                        }
                        break;
                    case 8:
                        nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with RNG";
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                DrawGraph.createAndShowGraph(rng, nameGraph, showNumbers);
                            }
                        });
                        break;
                    case 9:
                        //Analise RNG
                        minRoute = rng.getMinRoute();
                        graphDiameter = rng.getGraphDiameter();
                        maxClusteringCoefficient = rng.getMaxClusteringCoefficient();
                        sumClusteringCoefficient = rng.getSumClusteringCoefficient();
                        averageClusteringCoefficient = rng.getAverageClusteringCoefficient();
                        globalClusteringCoefficient = rng.getGlobalClusteringCoefficient();
                        averagePathLength = rng.getAveragePathLength();
                        energyCost = rng.getCost(radius);
                        s = "";
                        s += "\nAnalise do Grafo RNG com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
                        s += "\nMenor Rota: \t" + minRoute;
                        s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter;
                        s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient;
                        s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient;
                        s += "\nMédia das menores rotas (stretch): \t" + averagePathLength;
                        s += "\nCusto de Energia: \t" + energyCost + "\n";
                        System.out.println(s);
                        break;
                    case 10:
                        //Dijkstra RNG
                        System.out.print("Informa o nó da fonte: ");
                        dijkstraSourceNode = sc.nextInt();
                        System.out.print("Informa o nó de destino: ");
                        dijkstraDestinationNode = sc.nextInt();
                        dijkstraRoute = Dijkstra.getShortestPath(rng, dijkstraSourceNode, dijkstraDestinationNode);
                        if (dijkstraRoute.length > 0) {
                            System.out.println("A menor rota entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + "> é: " + Arrays.toString(dijkstraRoute));
                            System.out.print("Para mostrar a rota no JFrame aparte \"y\": ");
                            dijkstraOption = sc.next();
                            if (dijkstraOption.equals("y")) {
                                nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with RNG";
                                DrawGraph.createAndShowGraphForDijkstra(rng, nameGraph, showNumbers, dijkstraRoute);
                            }
                        } else {
                            System.out.println("Não existem rotas possíveis entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + ">");
                        }
                        break;
                    case 11:
                        nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with MST";
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                DrawGraph.createAndShowGraph(mst, nameGraph, showNumbers);
                            }
                        });
                        break;
                    case 12:
                        //Analise MST
                        minRoute = mst.getMinRoute();
                        graphDiameter = mst.getGraphDiameter();
                        maxClusteringCoefficient = mst.getMaxClusteringCoefficient();
                        sumClusteringCoefficient = mst.getSumClusteringCoefficient();
                        averageClusteringCoefficient = mst.getAverageClusteringCoefficient();
                        globalClusteringCoefficient = mst.getGlobalClusteringCoefficient();
                        averagePathLength = mst.getAveragePathLength();
                        energyCost = mst.getCost(radius);
                        s = "";
                        s += "\nAnalise do Grafo MST com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
                        s += "\nMenor Rota: \t" + minRoute;
                        s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter;
                        s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient;
                        s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient;
                        s += "\nMédia das menores rotas (stretch): \t" + averagePathLength;
                        s += "\nCusto de Energia: \t" + energyCost + "\n";
                        System.out.println(s);
                        break;    
                    case 13:
                        //Dijkstra MST
                        System.out.print("Informa o nó da fonte: ");
                        dijkstraSourceNode = sc.nextInt();
                        System.out.print("Informa o nó de destino: ");
                        dijkstraDestinationNode = sc.nextInt();
                        dijkstraRoute = Dijkstra.getShortestPath(mst, dijkstraSourceNode, dijkstraDestinationNode);
                        if (dijkstraRoute.length > 0) {
                            System.out.println("A menor rota entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + "> é: " + Arrays.toString(dijkstraRoute));
                            System.out.print("Para mostrar a rota no JFrame aparte \"y\": ");
                            dijkstraOption = sc.next();
                            if (dijkstraOption.equals("y")) {
                                nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with MST";
                                DrawGraph.createAndShowGraphForDijkstra(mst, nameGraph, showNumbers, dijkstraRoute);
                            }
                        } else {
                            System.out.println("Não existem rotas possíveis entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + ">");
                        }
                        break;
                    case 14:
                        nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with KNNG";
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                DrawGraph.createAndShowGraph(knng, nameGraph, showNumbers);
                            }
                        });
                        break;
                    case 15:
                        //Analise KNNG
                        minRoute = knng.getMinRoute();
                        graphDiameter = knng.getGraphDiameter();
                        maxClusteringCoefficient = knng.getMaxClusteringCoefficient();
                        sumClusteringCoefficient = knng.getSumClusteringCoefficient();
                        averageClusteringCoefficient = knng.getAverageClusteringCoefficient();
                        globalClusteringCoefficient = knng.getGlobalClusteringCoefficient();
                        averagePathLength = knng.getAveragePathLength();
                        energyCost = knng.getCost(radius);
                        s = "";
                        s += "\nAnalise do Grafo MST com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
                        s += "\nMenor Rota: \t" + minRoute;
                        s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter;
                        s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient;
                        s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient;
                        s += "\nMédia das menores rotas (stretch): \t" + averagePathLength;
                        s += "\nCusto de Energia: \t" + energyCost + "\n";
                        System.out.println(s);
                        break;    
                    case 16:
                        //Dijkstra KNNG
                        System.out.print("Informa o nó da fonte: ");
                        dijkstraSourceNode = sc.nextInt();
                        System.out.print("Informa o nó de destino: ");
                        dijkstraDestinationNode = sc.nextInt();
                        dijkstraRoute = Dijkstra.getShortestPath(knng, dijkstraSourceNode, dijkstraDestinationNode);
                        if (dijkstraRoute.length > 0) {
                            System.out.println("A menor rota entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + "> é: " + Arrays.toString(dijkstraRoute));
                            System.out.print("Para mostrar a rota no JFrame aparte \"y\": ");
                            dijkstraOption = sc.next();
                            if (dijkstraOption.equals("y")) {
                                nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with KNNG";
                                DrawGraph.createAndShowGraphForDijkstra(knng, nameGraph, showNumbers, dijkstraRoute);
                            }
                        } else {
                            System.out.println("Não existem rotas possíveis entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + ">");
                        }
                        break;
                    case 17:
                        nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with YG";
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                DrawGraph.createAndShowGraph(yg, nameGraph, showNumbers);
                            }
                        });
                        break;
                    case 18:
                        //Analise YG
                        minRoute = yg.getMinRoute();
                        graphDiameter = yg.getGraphDiameter();
                        maxClusteringCoefficient = yg.getMaxClusteringCoefficient();
                        sumClusteringCoefficient = yg.getSumClusteringCoefficient();
                        averageClusteringCoefficient = yg.getAverageClusteringCoefficient();
                        globalClusteringCoefficient = yg.getGlobalClusteringCoefficient();
                        averagePathLength = yg.getAveragePathLength();
                        energyCost = yg.getCost(radius);
                        s = "";
                        s += "\nAnalise do Grafo MST com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
                        s += "\nMenor Rota: \t" + minRoute;
                        s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter;
                        s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient;
                        s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient;
                        s += "\nMédia das menores rotas (stretch): \t" + averagePathLength;
                        s += "\nCusto de Energia: \t" + energyCost + "\n";
                        System.out.println(s);
                        break;    
                    case 19:
                        //Dijkstra YG
                        System.out.print("Informa o nó da fonte: ");
                        dijkstraSourceNode = sc.nextInt();
                        System.out.print("Informa o nó de destino: ");
                        dijkstraDestinationNode = sc.nextInt();
                        dijkstraRoute = Dijkstra.getShortestPath(yg, dijkstraSourceNode, dijkstraDestinationNode);
                        if (dijkstraRoute.length > 0) {
                            System.out.println("A menor rota entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + "> é: " + Arrays.toString(dijkstraRoute));
                            System.out.print("Para mostrar a rota no JFrame aparte \"y\": ");
                            dijkstraOption = sc.next();
                            if (dijkstraOption.equals("y")) {
                                nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with YG";
                                DrawGraph.createAndShowGraphForDijkstra(yg, nameGraph, showNumbers, dijkstraRoute);
                            }
                        } else {
                            System.out.println("Não existem rotas possíveis entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + ">");
                        }
                        break; 
                    case 20:
                        nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with GG";
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                DrawGraph.createAndShowGraph(gg, nameGraph, showNumbers);
                            }
                        });
                        break;
                    case 21:
                        //Analise GG
                        minRoute = gg.getMinRoute();
                        graphDiameter = gg.getGraphDiameter();
                        maxClusteringCoefficient = gg.getMaxClusteringCoefficient();
                        sumClusteringCoefficient = gg.getSumClusteringCoefficient();
                        averageClusteringCoefficient = gg.getAverageClusteringCoefficient();
                        globalClusteringCoefficient = gg.getGlobalClusteringCoefficient();
                        averagePathLength = gg.getAveragePathLength();
                        energyCost = gg.getCost(radius);
                        s = "";
                        s += "\nAnalise do Grafo MST com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
                        s += "\nMenor Rota: \t" + minRoute;
                        s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter;
                        s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient;
                        s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient;
                        s += "\nMédia das menores rotas (stretch): \t" + averagePathLength;
                        s += "\nCusto de Energia: \t" + energyCost + "\n";
                        System.out.println(s);
                        break;    
                    case 22:
                        //Dijkstra GG
                        System.out.print("Informa o nó da fonte: ");
                        dijkstraSourceNode = sc.nextInt();
                        System.out.print("Informa o nó de destino: ");
                        dijkstraDestinationNode = sc.nextInt();
                        dijkstraRoute = Dijkstra.getShortestPath(gg, dijkstraSourceNode, dijkstraDestinationNode);
                        if (dijkstraRoute.length > 0) {
                            System.out.println("A menor rota entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + "> é: " + Arrays.toString(dijkstraRoute));
                            System.out.print("Para mostrar a rota no JFrame aparte \"y\": ");
                            dijkstraOption = sc.next();
                            if (dijkstraOption.equals("y")) {
                                nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with GG";
                                DrawGraph.createAndShowGraphForDijkstra(gg, nameGraph, showNumbers, dijkstraRoute);
                            }
                        } else {
                            System.out.println("Não existem rotas possíveis entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + ">");
                        }
                        break; 
                    case 23:
                        nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with BLU";
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                DrawGraph.createAndShowGraph(blu, nameGraph, showNumbers);
                            }
                        });
                        break;
                    case 24:
                        //Analise BLU
                        minRoute = blu.getMinRoute();
                        graphDiameter = blu.getGraphDiameter();
                        maxClusteringCoefficient = blu.getMaxClusteringCoefficient();
                        sumClusteringCoefficient = blu.getSumClusteringCoefficient();
                        averageClusteringCoefficient = blu.getAverageClusteringCoefficient();
                        globalClusteringCoefficient = blu.getGlobalClusteringCoefficient();
                        averagePathLength = blu.getAveragePathLength();
                        energyCost = blu.getCost(radius);
                        s = "";
                        s += "\nAnalise do Grafo BLU com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
                        s += "\nMenor Rota: \t" + minRoute;
                        s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter;
                        s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient;
                        s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient;
                        s += "\nMédia das menores rotas (stretch): \t" + averagePathLength;
                        s += "\nCusto de Energia: \t" + energyCost + "\n";
                        System.out.println(s);
                        break;    
                    case 25:
                        //Dijkstra BLU
                        System.out.print("Informa o nó da fonte: ");
                        dijkstraSourceNode = sc.nextInt();
                        System.out.print("Informa o nó de destino: ");
                        dijkstraDestinationNode = sc.nextInt();
                        dijkstraRoute = Dijkstra.getShortestPath(blu, dijkstraSourceNode, dijkstraDestinationNode);
                        if (dijkstraRoute.length > 0) {
                            System.out.println("A menor rota entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + "> é: " + Arrays.toString(dijkstraRoute));
                            System.out.print("Para mostrar a rota no JFrame aparte \"y\": ");
                            dijkstraOption = sc.next();
                            if (dijkstraOption.equals("y")) {
                                nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with BLU";
                                DrawGraph.createAndShowGraphForDijkstra(blu, nameGraph, showNumbers, dijkstraRoute);
                            }
                        } else {
                            System.out.println("Não existem rotas possíveis entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + ">");
                        }
                        break;
                    case 26:
                        nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with BIP";
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                DrawGraph.createAndShowGraph(bip, nameGraph, showNumbers);
                            }
                        });
                        break;
                    case 27:
                        //Analise BIP
                        minRoute = bip.getMinRoute();
                        graphDiameter = bip.getGraphDiameter();
                        maxClusteringCoefficient = bip.getMaxClusteringCoefficient();
                        sumClusteringCoefficient = bip.getSumClusteringCoefficient();
                        averageClusteringCoefficient = bip.getAverageClusteringCoefficient();
                        globalClusteringCoefficient = bip.getGlobalClusteringCoefficient();
                        averagePathLength = bip.getAveragePathLength();
                        energyCost = bip.getCost(radius);
                        s = "";
                        s += "\nAnalise do Grafo BIP com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
                        s += "\nMenor Rota: \t" + minRoute;
                        s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter;
                        s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient;
                        s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient;
                        s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient;
                        s += "\nMédia das menores rotas (stretch): \t" + averagePathLength;
                        s += "\nCusto de Energia: \t" + energyCost + "\n";
                        System.out.println(s);
                        break;    
                    case 28:
                        //Dijkstra BIP
                        System.out.print("Informa o nó da fonte: ");
                        dijkstraSourceNode = sc.nextInt();
                        System.out.print("Informa o nó de destino: ");
                        dijkstraDestinationNode = sc.nextInt();
                        dijkstraRoute = Dijkstra.getShortestPath(bip, dijkstraSourceNode, dijkstraDestinationNode);
                        if (dijkstraRoute.length > 0) {
                            System.out.println("A menor rota entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + "> é: " + Arrays.toString(dijkstraRoute));
                            System.out.print("Para mostrar a rota no JFrame aparte \"y\": ");
                            dijkstraOption = sc.next();
                            if (dijkstraOption.equals("y")) {
                                nameGraph = "Grafo " + g.getSize() + "x" + g.getSize() + " with BLU";
                                DrawGraph.createAndShowGraphForDijkstra(bip, nameGraph, showNumbers, dijkstraRoute);
                            }
                        } else {
                            System.out.println("Não existem rotas possíveis entre os nós <" + dijkstraSourceNode + "," + dijkstraDestinationNode + ">");
                        }
                        break;
                    case 0:
                        System.out.println("Saindo do programa");
                        System.exit(0);
                        break;
                }
            }
        }
    }
}
