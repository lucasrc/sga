/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;

import java.io.IOException;
import mnsim.tc.BIP;
import mnsim.tc.BLU;
import mnsim.tc.RNG;
import mnsim.util.Distribution;
import mnsim.util.Graph;
import mnsim.tc.GG;
import mnsim.tc.KNNG;
import mnsim.tc.MST;
import mnsim.tc.NNG;
import mnsim.tc.UDG;
import mnsim.tc.YG;
import mnsim.tools.SaveTextFile;

/**
 *
 * @author lucas
 */
public class RMSimulatorMain1 {

    static int nGraphs;
    static String distribuition;
    static int seed;
    static int sizeGraph;
    static int nodes;
    static int radius;
    static int kNeighbor;
    static int kSector;
    static boolean verbose = false;
    static SaveTextFile file = null;
    
    public static void main2(String[] args) throws IOException {
        
        String usage = "Usage: RMSimulator <uniform, normal, poisson> number_of_graphs seed network_space number_of_nodes radius kNeighbors kSector [-verbose] [file]";
        if (args.length < 8 || args.length > 10) {
            System.out.println(usage);
            System.exit(0);
        }
        
        distribuition = args[0];
        nGraphs = Integer.parseInt(args[1]);
        seed = Integer.parseInt(args[2]);
        sizeGraph = Integer.parseInt(args[3]);
        nodes = Integer.parseInt(args[4]);
        radius = Integer.parseInt(args[5]);
        kNeighbor = Integer.parseInt(args[6]);
        kSector = Integer.parseInt(args[7]);
        
        if (args.length == 9 && args[8].equals("-verbose")) {
            verbose = true;
        } else {
            if (args.length == 9) {
                file = new SaveTextFile(args[8]);
            } else {
                if (args[8].equals("-verbose")) {
                    verbose = true;
                    file = new SaveTextFile(args[9]);
                } else {
                    System.out.println(usage);
                    System.exit(0);
                }
            }
        }
        if(file != null){
            file.print("Graph\tminRoute\tgraphDiameter\tglobalClusteringCoefficient\tstretch\tenergy\n");
        }
        
        Graph[] graphs = new Graph[nGraphs];
        long tempoInicio = System.currentTimeMillis();
        System.out.println("Inicio do Programa [" + (System.currentTimeMillis() - tempoInicio) + "ms]");
        if (distribuition.equals("uniform")) {
            if(verbose) System.out.println("Gerando os " + graphs.length + " grafos aleatórios (Distribuição Uniforme)");
            uniformDistribuition(graphs, seed, sizeGraph, nodes);
        }
        if (distribuition.equals("normal")) {
            if(verbose) System.out.println("Gerando os " + graphs.length + " grafos aleatórios (Distribuição Normal)");
            normalDistribuition(graphs, seed, sizeGraph, nodes);
        }
        if (distribuition.equals("poisson")) {
            if(verbose) System.out.println("Gerando os " + graphs.length + " grafos aleatórios (Distribuição Poissom)");
            poissonDistribuition(graphs, seed, sizeGraph, nodes);
        }
        if(verbose) System.out.println(nGraphs + " grafos aleatórios gerados [" + (System.currentTimeMillis() - tempoInicio) + "ms]");
        //Creating graphs Topology controls
        if(verbose) System.out.println("Criando Topologias UDG, NNG, RNG, MST, KNNG, YG, GG");
        Graph[] udgGraphs = new Graph[graphs.length];
        Graph[] nngGraphs = new Graph[graphs.length];
        Graph[] rngGraphs = new Graph[graphs.length];
        Graph[] mstGraphs = new Graph[graphs.length];
        Graph[] knngGraphs = new Graph[graphs.length];
        Graph[] ygGraphs = new Graph[graphs.length];
        Graph[] ggGraphs = new Graph[graphs.length];
        Graph[] bluGraphs = new Graph[graphs.length];
        Graph[] bipGraphs = new Graph[graphs.length];
        for (int i = 0; i < graphs.length; i++) {
            udgGraphs[i] = UDG.unitDiskGraph(graphs[i], radius);
            nngGraphs[i] = NNG.nearestNeighborGraph(graphs[i], radius);
            rngGraphs[i] = RNG.relativeNeighborhoodGraph(graphs[i], radius);
            mstGraphs[i] = MST.minimumSpanningTree(udgGraphs[i]);
            knngGraphs[i] = KNNG.knearestNeighborGraph(graphs[i], radius, kNeighbor);
            ygGraphs[i] = YG.yaoGraph(graphs[i], radius, kSector);
            ggGraphs[i] = GG.gabrielGraphv(udgGraphs[i]);
            bluGraphs[i] = BLU.broadcastLeastUnicastCost(udgGraphs[i]);
            bipGraphs[i] = BIP.broadcastIncreasePower(graphs[i], radius);
        }
        if(verbose) System.out.println("Topologias criadas [" + (System.currentTimeMillis() - tempoInicio) + "ms]");
        
        String s = "";
        if (verbose) {
            System.out.println("Fazendo analise do UDG");
            s = analysisGraphs("UDG",udgGraphs);
            System.out.println(s);
            System.out.println("Fim da Analise UDG [" + (System.currentTimeMillis() - tempoInicio) + "ms]");
        }
        if(file != null){
            file.print(analysisGraphsFile("UDG", udgGraphs));
        }
        
        s = "";
        if (verbose) {
            System.out.println("Fazendo analise do NNG");
            s = analysisGraphs("NNG",nngGraphs);
            System.out.println(s);
            System.out.println("Fim da Analise NNG [" + (System.currentTimeMillis() - tempoInicio) + "ms]");
        }
        if(file != null){
            file.print(analysisGraphsFile("NNG", nngGraphs));
        }
        
        s = "";
        if (verbose) {
            System.out.println("Fazendo analise do RNG");
            s = analysisGraphs("RNG",rngGraphs);
            System.out.println(s);
            System.out.println("Fim da Analise RNG [" + (System.currentTimeMillis() - tempoInicio) + "ms]");
        }
        if(file != null){
            file.print(analysisGraphsFile("RNG", rngGraphs));
        }
        
        s = "";
        if (verbose) {
            System.out.println("Fazendo analise do MST");
            s = analysisGraphs("MST",mstGraphs);
            System.out.println(s);
            System.out.println("Fim da Analise MST [" + (System.currentTimeMillis() - tempoInicio) + "ms]");
        }
        if(file != null){
            file.print(analysisGraphsFile("MST", mstGraphs));
        }
        
        s = "";
        if (verbose) {
            System.out.println("Fazendo analise do KNNG");
            s = analysisGraphs("KNNG com k="+kNeighbor,knngGraphs);
            System.out.println(s);
            System.out.println("Fim da Analise KNNG [" + (System.currentTimeMillis() - tempoInicio) + "ms]");
        }
        if(file != null){
            file.print(analysisGraphsFile("KNNG", knngGraphs));
        }
        
        s = "";
        if (verbose) {
            System.out.println("Fazendo analise do YG");
            s = analysisGraphs("KNNG com "+kSector+" setores", ygGraphs);
            System.out.println(s);
            System.out.println("Fim da Analise YG [" + (System.currentTimeMillis() - tempoInicio) + "ms]");
        }
        if(file != null){
            file.print(analysisGraphsFile("YG", ygGraphs));
        }
        
        s = "";
        if (verbose) {
            System.out.println("Fazendo analise do GG");
            s = analysisGraphs("GG", ggGraphs);
            System.out.println(s);
            System.out.println("Fim da Analise GG [" + (System.currentTimeMillis() - tempoInicio) + "ms]");
        }
        if(file != null){
            file.print(analysisGraphsFile("GG", ggGraphs));
        }
        
        if (verbose) {
            System.out.println("Fazendo analise do BLU");
            s = analysisGraphs("BLU", bluGraphs);
            System.out.println(s);
            System.out.println("Fim da Analise BLU [" + (System.currentTimeMillis() - tempoInicio) + "ms]");
        }
        if(file != null){
            file.print(analysisGraphsFile("BLU", bluGraphs));
        }
        
        if (verbose) {
            System.out.println("Fazendo analise do BIP");
            s = analysisGraphs("BIP", bipGraphs);
            System.out.println(s);
            System.out.println("Fim da Analise BIP [" + (System.currentTimeMillis() - tempoInicio) + "ms]");
        }
        if(file != null){
            file.print(analysisGraphsFile("BIP", bipGraphs));
        }
        
        //CONTINUAR AKI
        //TODO
        if(file != null){
            file.close();
        }
        System.out.println("Fim do programa [" + (System.currentTimeMillis() - tempoInicio) + "ms]");
    }

    private static void uniformDistribuition(Graph[] graphs, int seed, int sizeGraph, int nodes) {
        Distribution distX = new Distribution(1, seed);
        Distribution distY = new Distribution(2, seed);
        for (int i = 0; i < graphs.length; i++) {
            graphs[i] = new Graph(sizeGraph);
            for (int j = 0; j < nodes; j++) {
                int x0 = distX.nextInt(sizeGraph);
                int y0 = distY.nextInt(sizeGraph);
                while (graphs[i].isVertex(x0, y0)) {
                    x0 = distX.nextInt(sizeGraph);
                    y0 = distY.nextInt(sizeGraph);
                }
                graphs[i].addVertex(j, x0, y0);
            }
        }
    }

    private static void normalDistribuition(Graph[] graphs, int seed, int sizeGraph, int nodes) {
        Distribution distX = new Distribution(1, seed);
        Distribution distY = new Distribution(2, seed);
        for (int i = 0; i < graphs.length; i++) {
            graphs[i] = new Graph(sizeGraph);
            for (int j = 0; j < nodes; j++) {
                int x0 = (int) Math.round(distX.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                int y0 = (int) Math.round(distY.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                while (graphs[i].isVertex(x0, y0)) {
                    x0 = (int) Math.round(distX.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                    y0 = (int) Math.round(distY.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                }
                graphs[i].addVertex(j, x0, y0);
            }
        }
    }

    private static void poissonDistribuition(Graph[] graphs, int seed, int sizeGraph, int nodes) {
        Distribution distX = new Distribution(1, seed);
        Distribution distY = new Distribution(2, seed);
        for (int i = 0; i < graphs.length; i++) {
            graphs[i] = new Graph(sizeGraph);
            for (int j = 0; j < nodes; j++) {
                int x0 = (int) Math.round(distX.nextExponential(sizeGraph, 0, sizeGraph));
                int y0 = (int) Math.round(distY.nextExponential(sizeGraph, 0, sizeGraph));
                while (graphs[i].isVertex(x0, y0)) {
                    x0 = (int) Math.round(distX.nextExponential(sizeGraph, 0, sizeGraph));
                    y0 = (int) Math.round(distY.nextExponential(sizeGraph, 0, sizeGraph));
                }
                graphs[i].addVertex(j, x0, y0);
            }
        }
    }

    private static String analysisGraphs(String name, Graph[] graphs) {
        double minRoute = 0;
        double graphDiameter = 0;
        double maxClusteringCoefficient = 0;
        double sumClusteringCoefficient = 0;
        double averageClusteringCoefficient = 0;
        double globalClusteringCoefficient = 0;
        double averagePathLength = 0;
        double energyCost = 0;
        String s;
        for (Graph graph : graphs) {
            minRoute += graph.getMinRoute();
            graphDiameter += graph.getGraphDiameter();
            maxClusteringCoefficient += graph.getMaxClusteringCoefficient();
            sumClusteringCoefficient += graph.getSumClusteringCoefficient();
            averageClusteringCoefficient += graph.getAverageClusteringCoefficient();
            globalClusteringCoefficient += graph.getGlobalClusteringCoefficient();
            averagePathLength += graph.getAveragePathLength();
            energyCost += graph.getCost(radius);
        }
        s = "";
        s += "\nAnalise dos " + graphs.length + " grafos "+ name +" com <seed>=" + seed + " <network space>=" + sizeGraph + " <number of nodes>=" + nodes + " <radius>=" + radius;
        s += "\nMenor Rota: \t" + minRoute / (double) graphs.length;
        s += "\nDiametro de grafo (Maior das menores rotas): \t" + graphDiameter / (double) graphs.length;
        s += "\nMaior Coeficiente de agrupamento: \t" + maxClusteringCoefficient / (double) graphs.length;
        s += "\nSoma do Coeficiente de agrupamento: \t" + sumClusteringCoefficient / (double) graphs.length;
        s += "\nCoeficiente de agrupamento médio: \t" + averageClusteringCoefficient / (double) graphs.length;
        s += "\nCoeficiente de agrupamento Global: \t" + globalClusteringCoefficient / (double) graphs.length;
        s += "\nMédia das menores rotas (stretch): \t" + averagePathLength / (double) graphs.length;
        s += "\nCusto de Energia: \t" + energyCost / (double) graphs.length + "\n";
        return s;
    }
    
    private static String analysisGraphsFile(String name, Graph[] graphs) {
        double minRoute = 0;
        double graphDiameter = 0;
        double globalClusteringCoefficient = 0;
        double averagePathLength = 0;
        double energyCost = 0;
        String s;
        for (Graph graph : graphs) {
            minRoute += graph.getMinRoute();
            graphDiameter += graph.getGraphDiameter();
            globalClusteringCoefficient += graph.getGlobalClusteringCoefficient();
            averagePathLength += graph.getAveragePathLength();
            energyCost += graph.getCost(radius);
        }
        s = ""+name+"\t";
        s += minRoute / (double) graphs.length+"\t";
        s += graphDiameter / (double) graphs.length+"\t";
        s += globalClusteringCoefficient / (double) graphs.length+"\t";
        s += averagePathLength / (double) graphs.length+"\t";
        s += energyCost / (double) graphs.length+"\n";
        return s;
    }
}

