/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.tc;

import mnsim.util.Graph;

/**
 *
 * @author lucas
 */
public class NNG implements TC {

    @Override
    public Graph getTopology(Graph g, int radius, int k) {
        return nearestNeighborGraph(g, radius);
    }

    public static Graph nearestNeighborGraph(Graph g, int r) {
        Graph graph = new Graph(g);
        double d;
        double min;
        int vertexCandidate;
        double distanceCandidate;
        for (int i = 0; i < g.getNumberVertex(); i++) {
            min = Double.MAX_VALUE;
            vertexCandidate = -1;
            distanceCandidate = -1;
            for (int j = 0; j < g.getNumberVertex(); j++) {
                if (i != j) {
                    d = getDistance(graph, i, j);
                    if(d <= (double) r && d < min){
                        vertexCandidate = j;
                        distanceCandidate = d;
                        min = d;
                    }
                }
            }
            if(vertexCandidate >= 0){
                graph.addEdge(i, vertexCandidate, distanceCandidate);
                graph.addEdge(vertexCandidate, i, distanceCandidate);
            }
        }
        return graph;
    }

    private static double getDistance(Graph graph, int i, int j) {
        double distance = Math.sqrt((Math.pow((graph.getVertex(j).x - graph.getVertex(i).x), 2)+ Math.pow((graph.getVertex(j).y - graph.getVertex(i).y), 2)));
        return distance;
    }
}
