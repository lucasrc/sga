/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.tc;

import java.util.ArrayList;
import mnsim.util.Graph;

/**
 * This class returns the Relative Neighbor Graph.
 * 
 * The relative neighborhood graph (RNG) is an undirected graph defined on a set of points in 
 * the Euclidean plane by connecting two points p and q by an edge whenever there does not 
 * exist a third point r that is closer to both p and q than they are to each other. 
 * 
 * @author lucas
 */
public class RNG implements TC {

    @Override
    public Graph getTopology(Graph g, int radius, int k) {
        return relativeNeighborhoodGraph(g, radius);
    }
    
    public static Graph relativeNeighborhoodGraph(Graph g, int r) {
        //Do the UDG (original Graph)
        Graph graph = new Graph(g);
        double d;
        for (int i = 0; i < graph.getNumberVertex(); i++) {
            for (int j = i+1; j < graph.getNumberVertex(); j++) {
                if(i != j){
                    if(shouldConnect(graph, i, j)){
                        d = getDistance(graph, i, j);
                        if(d <= (double) r){
                            graph.addEdge(i, j, d);
                            graph.addEdge(j, i, d);
                        }
                    }
                }
            }
        }
        return graph;
    }
    
    private static double getDistance(Graph graph, int i, int j) {
        double distance = Math.sqrt((Math.pow((graph.getVertex(j).x - graph.getVertex(i).x), 2)+ Math.pow((graph.getVertex(j).y - graph.getVertex(i).y), 2)));
        return distance;
    }

    private static ArrayList<Integer> getCommonNodes(Graph graph, int source, int destination) {
        ArrayList<Integer> commonNodes = new ArrayList<>();
        for (int i = 0; i < graph.getVertex(source).getNumberEdges(); i++) {
            for (int j = 0; j < graph.getVertex(destination).getNumberEdges(); j++) {
                if (i != j) {
                    if (graph.getVertex(source).edge.get(i).destination.id == graph.getVertex(destination).edge.get(j).destination.id) {
                        commonNodes.add(graph.getVertex(source).edge.get(i).destination.id);
                    }
                }

            }
        }
        return commonNodes;
    }

    private static boolean shouldConnect(Graph graph, int i, int j) {
        double distance = getDistance(graph, i, j);
        for (int k = 0; k < graph.getNumberVertex(); k++) {
            if(graph.getVertex(k) != graph.getVertex(i) && graph.getVertex(k) != graph.getVertex(j)){
                if(getDistance(graph, i, graph.getVertex(k).id) < distance && getDistance(graph, j, graph.getVertex(k).id) < distance){
                    return false;
                }
            }
        }
        return true;
    }
}

