/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.tc;

import java.util.ArrayList;
import mnsim.util.Graph;
import mnsim.util.Graph.Edge;
import mnsim.util.Graph.Vertex;

/**
 *
 * @author lucas
 */
public class MST implements TC {

    @Override
    public Graph getTopology(Graph g, int radius, int k) {
        return minimumSpanningTree(UDG.unitDiskGraph(g, radius));
    }

    public static Graph minimumSpanningTree(Graph g) {
        //Graph g is UDG graph
        Graph graph = new Graph(g, true);
        double d;
        //Prim's algorithm
        ArrayList<Vertex> visitedVertex = new ArrayList<>();
        ArrayList<Vertex> unvisitedVertex = new ArrayList<>();
        Edge edge;
        
        visitedVertex.add(graph.getVertex(0));
        for (int i = 1; i < graph.getNumberVertex(); i++) {
            unvisitedVertex.add(graph.getVertex(i));
        }       
        while (!unvisitedVertex.isEmpty()) {
            //edge do grafo g
            edge = getLeastEdge(g, visitedVertex);
            if (edge != null) {
                unvisitedVertex.remove(graph.getVertex(edge.destination.id));
                graph.addEdge(edge.source.id, edge.destination.id, edge.weight);
                graph.addEdge(edge.destination.id, edge.source.id, edge.weight);
                visitedVertex.add(graph.getVertex(edge.destination.id));
            } else {
                visitedVertex.clear();
                visitedVertex.add(unvisitedVertex.get(0));
                unvisitedVertex.remove(unvisitedVertex.get(0));
            }
        }
        return graph;
    }

    private static Edge getLeastEdge(Graph graph, ArrayList<Vertex> visitedVertex) {
        Edge e = null;
        double d;
        double min = Double.MAX_VALUE;
        for (Vertex vertex : visitedVertex) {
            for (Edge edge : graph.getVertex(vertex.id).edge) {
                if (!contains(edge.destination.id, visitedVertex)) {
                    if (edge.weight < min) {
                        min = edge.weight;
                        e = edge;
                    }
                }
            }
        }
        return e;
    }

    private static boolean contains(int id, ArrayList<Vertex> visitedVertex) {
        for (Vertex vertex : visitedVertex) {
            if(vertex.id == id){
                return true;
            }
        }
        return false;
    }

}
