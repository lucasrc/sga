/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.tc;

import mnsim.util.Graph;

/**
 *
 * @author lucas
 */
public class YG implements TC{

    @Override
    public Graph getTopology(Graph g, int radius, int k) {
        return yaoGraph(g, radius, k);
    }

    public static Graph yaoGraph(Graph g, int r, int sectors) {
        Graph graph = new Graph(g);
        
        int[] nodeSector = new int[sectors];
        double[] nodeDistance = new double[sectors];
        double d;
        double angle;
        int auxSetor;
        for (int i = 0; i < g.getNumberVertex(); i++) {
            for (int j = 0; j < sectors; j++) {
                nodeSector[j] = -1;
                nodeDistance[j] = Double.MAX_VALUE;
            }
            for (int j = 0; j < g.getNumberVertex(); j++) {
                if (i != j) {
                    d = getDistance(graph, i, j);
                    if (d <= (double) r) {
                        if (graph.getVertex(i).y <= graph.getVertex(j).y) {
                            if (graph.getVertex(i).x <= graph.getVertex(j).x) {
                                angle = getAngle(graph, i, j);
                            } else {
                                angle = 180.0 - getAngle(graph, i, j);
                            }
                        } else {
                            if (graph.getVertex(i).x <= graph.getVertex(j).x) {
                                angle = 180.0 + getAngle(graph, i, j);
                            } else {
                                angle = 360.0 - getAngle(graph, i, j);
                            }
                        }
                        auxSetor = (int) Math.floor(angle / (360 / sectors));
                        if (nodeDistance[auxSetor] > d) {
                            nodeSector[auxSetor] = j;
                            nodeDistance[auxSetor] = d;
                        }
                    }
                }
            }
            for (int j = 0; j < sectors; j++) {
                if (nodeSector[j] != -1) {
                    graph.addEdge(i, nodeSector[j], nodeDistance[j]);
                    //graph.addEdge(nodeSector[j], i, nodeDistance[j]);
                }
            }
        }
        return graph;
    }

    private static double getDistance(Graph graph, int i, int j) {
        double distance = Math.sqrt((Math.pow((graph.getVertex(j).x - graph.getVertex(i).x), 2) + Math.pow((graph.getVertex(j).y - graph.getVertex(i).y), 2)));
        return distance;
    }

    private static double getAngle(Graph graph, int i, int j) {
        double H = getDistance(graph, i, j);
        double CO = Math.abs(graph.getVertex(i).y - graph.getVertex(j).y);
        double sen = CO/H;
        double arcseno = Math.asin(sen) * 180.0 / Math.PI;
        return arcseno;
    }
}
