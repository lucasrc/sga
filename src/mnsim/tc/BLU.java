/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.tc;

import mnsim.util.Dijkstra;
import mnsim.util.Graph;

/**
 *
 * @author lucas
 */
public class BLU implements TC {

    @Override
    public Graph getTopology(Graph g, int radius, int k) {
        return broadcastLeastUnicastCost(UDG.unitDiskGraph(g, radius));
    }
    
    public static Graph broadcastLeastUnicastCost(Graph g) {
        //Graph g is UDG graph
        Graph graph = new Graph(g, true);
        int[] route;
        for (int i = 0; i < g.getNumberVertex(); i++) {
            route = Dijkstra.getShortestPath(g, 0, i);
            if (route != null) {
                for (int k = 0; k < route.length - 1; k++) {
                    if (!graph.isEdge(route[k], route[k + 1])) {
                        graph.addEdge(route[k], route[k + 1], g.getWeight(route[k], route[k + 1]));
                        graph.addEdge(route[k + 1], route[k], g.getWeight(route[k + 1], route[k]));
                    }
                }
            }
        }
        return graph;
    }
}
