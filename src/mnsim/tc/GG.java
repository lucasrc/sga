/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.tc;
import mnsim.util.Graph;

/**
 *
 * @author lucas
 */
public class GG implements TC {

    @Override
    public Graph getTopology(Graph g, int radius, int k) {
        return gabrielGraphv(UDG.unitDiskGraph(g, radius));
    }

    public static Graph gabrielGraphv(Graph g) {
        //Graph g is UDG graph
        Graph graph = new Graph(g);
        double distance, raio;
        int midpointX, midpointY;
        for (int i = 0; i < graph.getNumberVertex(); i++) {
            for (int j = 0; j < graph.getNumberVertex(); j++) {
                if (graph.isEdge(i, j)) {
                    midpointX = Math.round((graph.getVertex(i).x + graph.getVertex(j).x) / 2);
                    midpointY = Math.round((graph.getVertex(i).y + graph.getVertex(j).y) / 2);
                    raio = (getDistance(graph, i, j)/2);
                    for (int k = 0; k < graph.getNumberVertex(); k++) {
                        if(k != i && k != j){
                            distance = getDistance(graph, k, midpointX, midpointY);
                            if (distance < raio) {
                                graph.removeEdge(i, j);
                                graph.removeEdge(j, i);
                            }
                        }
                    }
                }
            }
        }
        return graph;
    }
    
    private static double getDistance(Graph graph, int realNode, int positionX, int positionY) {
        double distance = Math.sqrt((Math.pow((positionX - graph.getVertex(realNode).x), 2)+ Math.pow((positionY - graph.getVertex(realNode).y), 2)));
        return distance;
    }
    
    private static double getDistance(Graph graph, int i, int j) {
        double distance = Math.sqrt((Math.pow((graph.getVertex(j).x - graph.getVertex(i).x), 2)+ Math.pow((graph.getVertex(j).y - graph.getVertex(i).y), 2)));
        return distance;
    }
    
}
