/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.tc;

import mnsim.util.Graph;

/**
 *
 * @author lucas
 */
public interface TC {
    
    public Graph getTopology(Graph g, int radius, int k);
    
}
