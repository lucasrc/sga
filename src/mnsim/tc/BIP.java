/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.tc;

import mnsim.util.Graph;
import mnsim.util.Graph.Vertex;

/**
 *
 * @author lucas
 */
public class BIP implements TC {

    @Override
    public Graph getTopology(Graph g, int radius, int k) {
        return broadcastIncreasePower(g, radius);
    }
    
    public static Graph broadcastIncreasePower(Graph g, int r) {
        Graph finalGraph = new Graph(g.getSize());
        Graph auxGraph = new Graph(g, true);
        double[] radiusVertex = new double[g.getNumberVertex()];
        
        for (int i = 0; i < radiusVertex.length; i++) {
            radiusVertex[i] = 0.0;
        }
        
        finalGraph.addVertex(auxGraph.getVertex(0).id, auxGraph.getVertex(0).x, auxGraph.getVertex(0).y);
        auxGraph.removeVertex(0);
        
        while(auxGraph.getNumberVertex() > 0){
            double minRadius = r;
            int minVertex = -1;
            int connectedVertex = -1;
            for (Vertex v : finalGraph.getVertex()) {
                int currentVertex = closer(auxGraph, v.x, v.y);
                double currentRadius = getDistance(auxGraph, currentVertex, v.x, v.y);
                if(currentRadius - radiusVertex[v.id] <= minRadius && currentRadius <= r){
                    minRadius = currentRadius - radiusVertex[v.id];
                    minVertex = currentVertex;
                    connectedVertex = v.id;
                }
            }
            if(minVertex < 0){
                finalGraph.addVertex(auxGraph.getVertex().get(0).id, auxGraph.getVertex().get(0).x, auxGraph.getVertex().get(0).y);
                auxGraph.removeVertex(auxGraph.getVertex().get(0).id);
            } else {
                finalGraph.addVertex(auxGraph.getVertex(minVertex).id, auxGraph.getVertex(minVertex).x, auxGraph.getVertex(minVertex).y);
                radiusVertex[connectedVertex] += minRadius;
                finalGraph.addEdge(connectedVertex, minVertex, radiusVertex[connectedVertex]);
                auxGraph.removeVertex(minVertex);
            }
        }
        return finalGraph;
    }
    
    private static double getDistance(Graph graph, int i, int x, int y) {
        double distance = Math.sqrt((Math.pow((x - graph.getVertex(i).x), 2)+ Math.pow((y - graph.getVertex(i).y), 2)));
        return distance;
    }

    private static int closer(Graph auxGraph, int x, int y) {
        double min = Double.MAX_VALUE;
        double d;
        int vertex = -1;
        for (Vertex v : auxGraph.getVertex()) {
            d = getDistance(auxGraph, v.id, x, y);
            if (d < min) {
                min = d;
                vertex = v.id;
            }

        }
        return vertex;
    }

}

