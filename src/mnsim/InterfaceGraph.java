/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;

import java.util.Arrays;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import mnsim.tc.BIP;
import mnsim.tc.BLU;
import mnsim.tc.GG;
import mnsim.tc.KNNG;
import mnsim.tc.MST;
import mnsim.tc.NNG;
import mnsim.tc.RNG;
import mnsim.tc.UDG;
import mnsim.tc.YG;
import mnsim.tools.DrawGraph;
import mnsim.util.Dijkstra;
import mnsim.util.Distribution;
import mnsim.util.Graph;

/**
 *
 * @author k4io_
 */
public class InterfaceGraph {

    private String type;
    private int nGraphs;
    private String distribuition;
    private int seed;
    private int sizeGraph;
    private int nodes;
    private int radius;
    private int kNeighbor;
    private int kSector;
    private int ct;
    private Graph g;
    
    public InterfaceGraph(String[] args) {
        
            type = "";
            nGraphs = 1;
            distribuition = args[0];
            seed = Integer.parseInt(args[1]);
            sizeGraph = Integer.parseInt(args[2]);
            nodes = Integer.parseInt(args[3]);
            radius = Integer.parseInt(args[4]);
            ct = Integer.parseInt(args[5]);
            kNeighbor = Integer.parseInt(args[6]);
            kSector = Integer.parseInt(args[7]); 
        
        g = new Graph(sizeGraph);
        if (distribuition.equals("uniform")) {
            System.out.println("Gerando um grafo aleatório (Distribuição Uniforme)");
            Distribution distX = new Distribution(1, seed);
            Distribution distY = new Distribution(2, seed);
            for (int i = 0; i < nodes; i++) {
                int x0 = distX.nextInt(sizeGraph);
                int y0 = distY.nextInt(sizeGraph);
                while (g.isVertex(x0, y0)) {
                    x0 = distX.nextInt(sizeGraph);
                    y0 = distY.nextInt(sizeGraph);
                }
                g.addVertex(i, x0, y0);
            }
        }
        if (distribuition.equals("normal")) {
            System.out.println("Gerando um grafo aleatório (Distribuição Normal)");
            Distribution distX = new Distribution(1, seed);
            Distribution distY = new Distribution(2, seed);
            for (int i = 0; i < nodes; i++) {
                int x0 = (int) Math.round(distX.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                int y0 = (int) Math.round(distY.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                while (g.isVertex(x0, y0)) {
                    x0 = (int) Math.round(distX.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                    y0 = (int) Math.round(distY.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                }
                g.addVertex(i, x0, y0);
            }
        }
        if (distribuition.equals("poisson")) {
            System.out.println("Gerando um grafo aleatório (Distribuição Poissom)");
            Distribution distX = new Distribution(1, seed);
            Distribution distY = new Distribution(2, seed);
            for (int i = 0; i < nodes; i++) {
                int x0 = (int) Math.round(distX.nextExponential(sizeGraph, 0, sizeGraph));
                int y0 = (int) Math.round(distY.nextExponential(sizeGraph, 0, sizeGraph));
                while (g.isVertex(x0, y0)) {
                    x0 = (int) Math.round(distX.nextExponential(sizeGraph, 0, sizeGraph));
                    y0 = (int) Math.round(distY.nextExponential(sizeGraph, 0, sizeGraph));
                }
                g.addVertex(i, x0, y0);
            }
        }
    }
    
    public DrawGraph getGraph(boolean showIds, boolean sizeWave, boolean comp){
        Graph udg = UDG.unitDiskGraph(g, radius);
        Graph graph = udg;
        boolean isTree = false;
        switch(ct){
            case 0:
                graph = BIP.broadcastIncreasePower(g, radius);        
                isTree = true;
                break;
            case 1:
                graph = BLU.broadcastLeastUnicastCost(udg);
                isTree = true;
                break;
            case 2:
                graph = GG.gabrielGraphv(udg);
                break;
            case 3:
                graph = KNNG.knearestNeighborGraph(g, radius, kNeighbor);
                break;
            case 4:
                graph = MST.minimumSpanningTree(udg);
                isTree = true;
                break;
            case 5:
                graph = NNG.nearestNeighborGraph(g, radius);
                break;
            case 6:
                graph = RNG.relativeNeighborhoodGraph(g, radius);
                break;
            case 7:
                graph = UDG.unitDiskGraph(g, radius);
                break;
            case 8:
                graph = YG.yaoGraph(g, radius, kSector);
                break;
        }
        if(comp){
            if(sizeWave){
                return DrawGraph.createAndShowGraph(graph, showIds, radius, isTree, comp);
            }else{
                return DrawGraph.createAndShowGraph(graph, showIds, 0, isTree, comp);
            }
        }else{
            if(sizeWave){
                return DrawGraph.createAndShowGraph(graph, showIds, radius, isTree, comp);
            }else{
                return DrawGraph.createAndShowGraph(graph, showIds, 0, isTree, comp);
            }
        }
    }
    
    public DrawGraph getGraph(boolean showIds, boolean sizeWave, int soucer, int destination){
        Graph udg = UDG.unitDiskGraph(g, radius);
        Graph graph = udg;
        int[] dijkstraRoute;
        boolean isTree = false;
        switch(ct){
            case 0:
                graph = BIP.broadcastIncreasePower(g, radius);
                isTree = true;
                break;
            case 1:
                graph = BLU.broadcastLeastUnicastCost(udg);
                isTree = true;
                break;
            case 2:
                graph = GG.gabrielGraphv(udg);
                break;
            case 3:
                graph = KNNG.knearestNeighborGraph(g, radius, kNeighbor);
                break;
            case 4:
                graph = MST.minimumSpanningTree(udg);
                isTree = true;
                break;
            case 5:
                graph = NNG.nearestNeighborGraph(g, radius);
                break;
            case 6:
                graph = RNG.relativeNeighborhoodGraph(g, radius);
                break;
            case 7:
                graph = UDG.unitDiskGraph(g, radius);
                break;
            case 8:
                graph = YG.yaoGraph(g, radius, kSector);
                break;
        }
        dijkstraRoute = Dijkstra.getShortestPath(graph, soucer, destination);
        if(sizeWave){
            return DrawGraph.createAndShowGraphForDijkstra(graph, showIds, radius, dijkstraRoute, isTree);
        }else{
            return DrawGraph.createAndShowGraphForDijkstra(graph, showIds, 0, dijkstraRoute, isTree);
        }
    }
    
}


