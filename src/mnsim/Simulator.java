/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *
 * @author lucas
 */
public class Simulator {
    
    private static String simName = "mnsim";
    private static final Float simVersion = (float) 0.1;
    public static boolean verbose = false;
    public static boolean trace = false;
    
    void Execute(String simConfigFile, boolean trace, boolean verbose, int seedTraffic, int seedGraph) {
        Simulator.verbose = verbose;
        Simulator.trace = trace;
        
        if (Simulator.verbose) {
            System.out.println("########################################################");
            System.out.println("# Mobile Networks Simulator - version " + simVersion.toString() + "  #");
            System.out.println("#######################################################\n");
        }
        
        try {

            long begin = System.currentTimeMillis();
            
            if (Simulator.verbose) {
                System.out.println("(0) Accessing simulation file " + simConfigFile + "...");
            }
            
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new File(simConfigFile));

            // normalize text representation
            doc.getDocumentElement().normalize();
            
            // check the root TAG name and version
            if (!doc.getDocumentElement().getNodeName().equals(simName)) {
                System.out.println("Root element of the simulation file is " + doc.getDocumentElement().getNodeName() + ", " + simName + " is expected!");
                System.exit(0);
            }
            if (!doc.getDocumentElement().hasAttribute("version")) {
                System.out.println("Cannot find version attribute!");
                System.exit(0);
            }
            if (Float.compare(new Float(doc.getDocumentElement().getAttribute("version")), simVersion) > 0) {
                System.out.println("Simulation config file requires a newer version of the simulator!");
                System.exit(0);
            }
            if (Simulator.verbose) {
                System.out.println("(0) Done. (" + Float.toString((float) ((float) (System.currentTimeMillis() - begin) / (float) 1000)) + " sec)\n");
            }
            /*
             * Create the Topology
             */
            begin = System.currentTimeMillis();
            Element xmlTOP = (Element) doc.getElementsByTagName("topology").item(0);
            if (Simulator.verbose) {
                String s = "(1) Creating topology ";
                s += xmlTOP.getAttribute("size")+"x"+xmlTOP.getAttribute("size");
                s += " in ";
                s += xmlTOP.getAttribute("distribuition");
                s += " distribuition with ";
                s += xmlTOP.getAttribute("nodes");
                s += " nodes and radius=";
                s += xmlTOP.getAttribute("radius");
                s += " and ";
                s += xmlTOP.getAttribute("channels");
                s += " channels";
                s += "...";
                System.out.println(s);
            }
            
            Element xmlTC = (Element) doc.getElementsByTagName("tc").item(0);
            String tcModule = "mnsim.tc." + xmlTC.getAttribute("module");
            if (Simulator.verbose) {
                System.out.println("Topology control module: " + tcModule);
            }
            
            
            
            Topology top = new Topology(xmlTOP, xmlTC, seedGraph, verbose);
            
            if (Simulator.verbose) {
                System.out.println(top);
            }
            if (Simulator.verbose) {
                System.out.println("(1) Done. (" + Float.toString((float) ((float) (System.currentTimeMillis() - begin) / (float) 1000)) + " sec)\n");
            }
            
            /*
             * Extract simulation traffic Module
             */
            begin = System.currentTimeMillis();
            if (Simulator.verbose) {
                System.out.println("(3) Loading traffic module...");
            }
            String tafficModule = "mnsim.traffic." + ((Element) doc.getElementsByTagName("traffic").item(0)).getAttribute("module");
            if (Simulator.verbose) {
                System.out.println("Traffic module: " + tafficModule);
            }
            
            EventScheduler events = new EventScheduler();
            TrafficGenerator traffic = new TrafficGenerator((Element) doc.getElementsByTagName("traffic").item(0));
            traffic.generateTraffic(top, events, seedTraffic);
            
            if (Simulator.verbose) {
                System.out.println("(3) Done. (" + Float.toString((float) ((float) (System.currentTimeMillis() - begin) / (float) 1000)) + " sec)\n");
            }
            
            /*
             * Extract simulation setup part
             */
            begin = System.currentTimeMillis();
            if (Simulator.verbose) {
                System.out.println("(4) Loading simulation setup information...");
            }
            
            int statisticStart = 0;
            if(((Element) doc.getElementsByTagName("traffic").item(0)).hasAttribute("statisticStart")){
                statisticStart = Integer.parseInt(((Element) doc.getElementsByTagName("traffic").item(0)).getAttribute("statisticStart"));
            }
            
            MyStatistics st = MyStatistics.getMyStatisticsObject();
            st.statisticsSetup(top, statisticStart);//Atencao no ultimo argumento, serve para coletar as estatisticas somente apos certo numero de chamdas
            
            Tracer tr = Tracer.getTracerObject();
            if (Simulator.trace == true)
            {
            	tr.setTraceFile(simConfigFile.substring(0, simConfigFile.length() - 4) + ".trace");
            }
            tr.toogleTraceWriting(Simulator.trace);
            
            Element xmlRA = (Element) doc.getElementsByTagName("ra").item(0);
            String raModule = "mnsim.ra." + ((Element) doc.getElementsByTagName("ra").item(0)).getAttribute("module");
            if (Simulator.verbose) {
                System.out.println("Routing algorithm module: " + raModule);
            }
            
            Element xmlMAC = (Element) doc.getElementsByTagName("mac").item(0);
            String macModule = "mnsim.mac." + xmlMAC.getAttribute("module");
            if (Simulator.verbose) {
                System.out.println("Media access control module: " + macModule);
            }
            
            ControlMedium cp = new ControlMedium(xmlRA, xmlMAC, top, events);

            if (Simulator.verbose) {
                System.out.println("(4) Done. (" + Float.toString((float) ((float) (System.currentTimeMillis() - begin) / (float) 1000)) + " sec)\n");
            }
            
            /*
             * Run the simulation
             */
            begin = System.currentTimeMillis();
            if (Simulator.verbose) {
                System.out.println("(5) Running the simulation...");
            }

            SimulationRunner sim = new SimulationRunner(cp, events);

            if (Simulator.verbose) {
                System.out.println("(5) Done. (" + Float.toString((float) ((float) (System.currentTimeMillis() - begin) / (float) 1000)) + " sec)\n");
            }
            
            if (Simulator.verbose) {
                System.out.println("Statistics (" + simConfigFile + "):\n");
                System.out.println(st.fancyStatistics());
            } else {
                System.out.println("*****");
                st.printStatistics();
            }
            
            // Terminate MyStatistics singleton
            st.finish();

            // Flush and close the trace file and terminate the singleton
            if (Simulator.trace == true)
            	tr.finish();
            
            
        } catch (SAXParseException err) {
            System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
            System.out.println(" " + err.getMessage());

        } catch (SAXException e) {
            Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
}
