/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;

/**
 *
 * @author lucas
 */
public class Packet {
    
    private long id;
    private int source;
    private int destination;
    private int type;

    public Packet(long id, int source, int destination, int type) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public int getSource() {
        return source;
    }

    public int getDestination() {
        return destination;
    }

    public int getType() {
        return type;
    }
}
