/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;

/**
 *
 * @author lucas
 */
public class PacketDepartureEvent extends Event {

    private long id;

    public PacketDepartureEvent(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
    
}
