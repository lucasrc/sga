/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;

import mnsim.traffic.Traffic;
import org.w3c.dom.Element;

/**
 *
 * @author lucas
 */
public class TrafficGenerator {
    
    private int calls;
    private double load;
    private Traffic trafficStyle;
    private int bandwidthBound;

    TrafficGenerator(Element xml) {
        Class TrafficClass;
        String trafficModule = "mnsim.traffic." + xml.getAttribute("module");
        try {
            TrafficClass = Class.forName(trafficModule);
            trafficStyle = (Traffic) TrafficClass.newInstance();
        } catch (Throwable t) {
            t.printStackTrace();
        }
        this.calls = Integer.parseInt(xml.getAttribute("calls"));
        trafficStyle.setCalls(calls);
        if(xml.hasAttribute("load")){
            load = Integer.parseInt(xml.getAttribute("load"));
        } else {
            load = 0;
        }
        trafficStyle.setLoad(load);
    }

    void generateTraffic(Topology top, EventScheduler events, int seed) {
        trafficStyle.setSeed(seed);
        events = trafficStyle.getEvents(top);
    }
    
}
