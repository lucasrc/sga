/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;

import mnsim.tc.TC;
import mnsim.tc.UDG;
import mnsim.util.Distribution;
import org.w3c.dom.Element;
import mnsim.util.Graph;
import mnsim.util.WeightedGraph;

/**
 *
 * @author lucas
 */
public class Topology {
    
    private static Graph graph;
    private static Graph neighborsGraph;
    private static Node[] nodes;
    private TC tc;

    Topology(Element xmlTOP, Element xmlTC, int seed, boolean verbose) {
        Class TCClass;
        
        int radius = Integer.parseInt(xmlTOP.getAttribute("radius"));
        int k = -1;
        
        String distribuition = xmlTOP.getAttribute("distribuition");
        int sizeGraph = Integer.parseInt(xmlTOP.getAttribute("size"));
        int numberNodes = Integer.parseInt(xmlTOP.getAttribute("nodes"));
        int channels = Integer.parseInt(xmlTOP.getAttribute("channels"));
        
        if(xmlTC.hasAttribute("kNeighbors")){
            k = Integer.parseInt(xmlTC.getAttribute("kNeighbors"));
        } else {
            if(xmlTC.hasAttribute("kSector")) {
                k = Integer.parseInt(xmlTC.getAttribute("kSector"));
            }
        }
        
        try {
            String tcModule = "mnsim.tc." + xmlTC.getAttribute("module");
            TCClass = Class.forName(tcModule);
            tc = (TC) TCClass.newInstance();
        } catch (Throwable t) {
            t.printStackTrace();
        }
        
        //Creates de randon Graph
        Graph auxGraph = null;
        if (distribuition.equals("uniform")) {
            if(verbose) System.out.println("Generate random graph (Uniform distribution)");
            uniformDistribuition(auxGraph, seed, sizeGraph, numberNodes);
        }
        if (distribuition.equals("normal")) {
            if(verbose) System.out.println("Generate random graph (Normal distribution)");
            normalDistribuition(auxGraph, seed, sizeGraph, numberNodes);
        }
        if (distribuition.equals("poisson")) {
            if(verbose) System.out.println("Generate random graph (Poisson distribution)");
            poissonDistribuition(auxGraph, seed, sizeGraph, numberNodes);
        }
        
        //Creates edges according Topology Control
        graph = tc.getTopology(auxGraph, radius, k);
        
        //Creates the udg graphs to determinates the neighbors of each node
        neighborsGraph = UDG.unitDiskGraph(auxGraph, radius);
        
        //Creates List of Nodes
        for (int i = 0; i < numberNodes; i++) {
            nodes[i] = new Node(i, radius, channels);
        }
       
    }
    
    public int getNumNodes() {
        return nodes.length;
    }
    
    public Node getNode(int id) {
        return nodes[id];
    }
    
    public boolean isEdge(int source, int destination){
        return graph.isEdge(source, destination);
    }
    
    public boolean isNode(int x, int y){
        return graph.isVertex(x, y);
    }
    
    public double getWeight(int source, int destination){
        return graph.getWeight(source, destination);
    }
    
    public Node[] neighbors(int id) {
        int[] neighbors = graph.neighbors(id);
        Node[] neighborsNodes = new Node[neighbors.length];
        for (int i = 0; i < neighbors.length; i++) {
            neighborsNodes[i] = nodes[neighbors[i]];
        }
        return neighborsNodes;
    }
    
    public Graph getGraph() {
        Graph g = new Graph(graph);
        return g;
    }
    
    public Graph getNeighborsGraph() {
        Graph g = new Graph(neighborsGraph);
        return g;
    }
    
    public WeightedGraph getWeightedGraph() {
        WeightedGraph g = new WeightedGraph(nodes.length);
        for (int i = 0; i < g.size(); i++) {
            for (int j = 0; j < g.size(); j++) {
                if (isEdge(i, j)) {
                    g.addEdge(i, j, getWeight(i, j));
                }
            }
        }
        return g;
    }
    
    private static void uniformDistribuition(Graph graph, int seed, int sizeGraph, int nodes) {
        Distribution distX = new Distribution(1, seed);
        Distribution distY = new Distribution(2, seed);
        graph = new Graph(sizeGraph);
        for (int i = 0; i < nodes; i++) {
            int x0 = distX.nextInt(sizeGraph);
            int y0 = distY.nextInt(sizeGraph);
            while (graph.isVertex(x0, y0)) {
                x0 = distX.nextInt(sizeGraph);
                y0 = distY.nextInt(sizeGraph);
            }
            graph.addVertex(i, x0, y0);
        }
    }

    private static void normalDistribuition(Graph graph, int seed, int sizeGraph, int nodes) {
        Distribution distX = new Distribution(1, seed);
        Distribution distY = new Distribution(2, seed);
        graph = new Graph(sizeGraph);
        for (int i = 0; i < nodes; i++) {
            int x0 = (int) Math.round(distX.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
            int y0 = (int) Math.round(distY.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
            while (graph.isVertex(x0, y0)) {
                x0 = (int) Math.round(distX.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
                y0 = (int) Math.round(distY.getRandomGaussium(sizeGraph / 2, 0, sizeGraph));
            }
            graph.addVertex(i, x0, y0);
        }
    }

    private static void poissonDistribuition(Graph graph, int seed, int sizeGraph, int nodes) {
        Distribution distX = new Distribution(1, seed);
        Distribution distY = new Distribution(2, seed);
        graph = new Graph(sizeGraph);
        for (int i = 0; i < nodes; i++) {
            int x0 = (int) Math.round(distX.nextExponential(sizeGraph, 0, sizeGraph));
            int y0 = (int) Math.round(distY.nextExponential(sizeGraph, 0, sizeGraph));
            while (graph.isVertex(x0, y0)) {
                x0 = (int) Math.round(distX.nextExponential(sizeGraph, 0, sizeGraph));
                y0 = (int) Math.round(distY.nextExponential(sizeGraph, 0, sizeGraph));
            }
            graph.addVertex(i, x0, y0);
        }
    }
    
}
