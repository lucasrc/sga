/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.traffic;

import mnsim.Defines;
import mnsim.PacketArrivalEvent;
import mnsim.Event;
import mnsim.EventScheduler;
import mnsim.Packet;
import mnsim.Topology;
import mnsim.util.Distribution;

/**
 *
 * @author lucas
 */
public class CBR extends Traffic {

    public CBR() {
    }

    @Override
    public EventScheduler getEvents(Topology top) {
        EventScheduler events = new EventScheduler();
        Event event;
        Distribution dist1;
        dist1 = new Distribution(1, seed);
        int src, dst;
        long id = 1;
        
        for (int i = 0; i < calls; i++) {
            for (int j = 0; j < top.getNumNodes(); j++) {
                src = j;
                dst = dist1.nextInt(top.getNumNodes());
                while (src == dst) {
                    dst = dist1.nextInt(top.getNumNodes());
                }
                event = new PacketArrivalEvent(new Packet(id++, src, dst, Defines.PacketDataType));
                event.setTime(i); //em cada us segundo sera criada uma chamada para cada no
                events.addEvent(event);
            }
        }
        return events;
    }
    
}
