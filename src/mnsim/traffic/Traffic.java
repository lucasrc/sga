/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.traffic;

import mnsim.EventScheduler;
import mnsim.Topology;

/**
 *
 * @author lucas
 */
public abstract class Traffic {
    
    protected int calls;
    protected double load;
    protected int seed;
    
    public Traffic() {
    }

    public int getCalls() {
        return calls;
    }

    public void setCalls(int calls) {
        this.calls = calls;
    }

    public double getLoad() {
        return load;
    }

    public void setLoad(double load) {
        this.load = load;
    }

    public int getSeed() {
        return seed;
    }

    public void setSeed(int seed) {
        this.seed = seed;
    }

    public abstract EventScheduler getEvents(Topology top);
    
}
