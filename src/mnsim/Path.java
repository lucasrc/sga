/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;

/**
 *
 * @author lucas
 */
public class Path {
    
    int path[];
    int channel;

    public Path(int[] path, int channel) {
        this.path = path;
        this.channel = channel;
    }

    public int[] getPath() {
        return path;
    }
    
}
