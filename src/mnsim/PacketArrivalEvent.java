/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;


/**
 *
 * @author lucas
 */
public class PacketArrivalEvent extends Event {

    private Packet pack;

    public PacketArrivalEvent(Packet pack) {
        this.pack = pack;
    }

    public Packet getPack() {
        return pack;
    }
    
}
