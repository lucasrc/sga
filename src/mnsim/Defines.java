/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;

/**
 *
 * @author lucas
 */
public class Defines {
    
    public static final int MAC_HEADER = 272; //bits
    public static final int PHY_HEADER = 128; //bits
    public static final int MAX_THROUGHPUT = 1; // 1 bit per us
    public static final int PROPAGATION_DELAY = 1; //us
    
    public static final int FAIL = -1;
    public static final int BROADCAST = 666;
    
    public static final int ACKTIMEOUT = 35; //us
    public static final int SLOT_TIME = 20; //us
    public static final int SIFS_TIME = 10; //us
    public static final int DIFS_TIME = 2*SLOT_TIME+SIFS_TIME; //us
    
    public static final int IdleState = 0;
    public static final int ListeningState = 1;
    public static final int SendingState = 2;
    public static final int NAVState = 3;
    
    public static final int PacketRTSType = 0;
    public static final int PacketCTSType = 1;
    public static final int PacketDataType = 2;
    public static final int PacketACKType = 3;
    
    public static final int DATA_LENGHT = 8184; //bits
    public static double DATA_TIME = Defines.getTime(DATA_LENGHT);
    
    public static final int ACK_LENGHT = 112;
    public static final double ACK_TIME = Defines.getTime(ACK_LENGHT);
    
    public static final double CTS_TIME = 200.0;
    public static final int CTS_LENGHT = Defines.getLenght(CTS_TIME);
    
    public static final double RTS_TIME = 200.0;
    public static final int RTS_LENGHT = Defines.getLenght(RTS_TIME);
    
    public static double getTime(int packetLenght){
        double time = PROPAGATION_DELAY + Math.ceil((PHY_HEADER + packetLenght)/MAX_THROUGHPUT);
        return time;
    }
    
    public static int getLenght(double packetTime){
        int lenght = (int) (Math.floor(((packetTime-PROPAGATION_DELAY)*MAX_THROUGHPUT) - PHY_HEADER));
        return lenght;
    }
}
