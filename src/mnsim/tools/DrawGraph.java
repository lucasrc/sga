/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.tools;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import mnsim.util.Graph;

/**
 *
 * @author lucas
 */
@SuppressWarnings("serial")
public class DrawGraph extends JPanel {

    private final int PREF_W;
    private final int PREF_H;
    private static final int BORDER_GAP = 30;
    private static final Color GRAPH_COLOR = Color.blue;
    private static final Color GRAPH_POINT_COLOR = new Color(150, 50, 50, 180);
    private static final Stroke GRAPH_STROKE = new BasicStroke(1.5f);
    private static final Stroke GRAPH_STROKE_SOURCE = new BasicStroke(5f);
    private static final Stroke STROKE_ROUTE = new BasicStroke(3f);
    private static final int GRAPH_POINT_WIDTH = 7;
    //private static final int GRAPH_POINT_WIDTH = 10;
    private final int xTick;
    private final int yTick;
    private final Graph graph;
    private final int size;
    private final int[] route;
    private final boolean showIds;
    private final boolean isTree;
    private static String name;
    private final int sizeWave;

    public DrawGraph(Graph graph, int size, boolean showIds) {
        this.graph = graph;
        this.size = size;
        this.xTick = (int) size / 10;
        this.yTick = this.xTick;
        this.showIds = showIds;
        this.route = new int[0];
        this.sizeWave = 0;
        this.isTree = false;
        this.PREF_H = 600;
        this.PREF_W = 600;
    }

    public DrawGraph(Graph graph, int size, boolean showIds, int[] route) {
        this.graph = graph;
        this.size = size;
        this.xTick = (int) size / 10;
        this.yTick = this.xTick;
        this.showIds = showIds;
        this.route = route;
        this.sizeWave = 0;
        this.isTree = false;
        this.PREF_H = 600;
        this.PREF_W = 600;
    }

    public DrawGraph(Graph graph, int size, boolean showIds, int sizeWave, boolean isTree) {
        this.graph = graph;
        this.size = size;
        this.xTick = (int) size / 10;
        this.yTick = this.xTick;
        this.showIds = showIds;
        this.route = new int[0];
        this.sizeWave = sizeWave;
        this.isTree = isTree;
        this.PREF_H = 600;
        this.PREF_W = 600;
    }

    public DrawGraph(Graph graph, int size, boolean showIds, int[] route, int sizeWave, boolean isTree) {
        this.graph = graph;
        this.size = size;
        this.xTick = (int) size / 10;
        this.yTick = this.xTick;
        this.showIds = showIds;
        this.route = route;
        this.sizeWave = sizeWave;
        this.isTree = isTree;
        this.PREF_H = 600;
        this.PREF_W = 600;
    }

    public DrawGraph(Graph graph, int size, boolean showIds, int sizeWave, boolean isTree, int mod) {
        this.graph = graph;
        this.size = size;
        this.xTick = (int) size / 10;
        this.yTick = this.xTick;
        this.showIds = showIds;
        this.route = new int[0];
        this.sizeWave = sizeWave;
        this.isTree = isTree;
        this.PREF_H = mod;
        this.PREF_W = mod;
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        Graph auxGraph = new Graph(graph, true);

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Line2D line = new Line2D.Double();

        double xScale = ((double) getWidth() - (2 * BORDER_GAP) + 1) / size;
        double yScale = ((double) getHeight() - (2 * BORDER_GAP) + 1) / size;

        List<Point> graphPointsInGraph = new ArrayList<Point>();
        for (int i = 0; i < graph.getNumberVertex(); i++) {
            int x1 = (int) (BORDER_GAP + graph.getVertex(i).x * xScale);
            int y1 = (int) ((getHeight() - BORDER_GAP) - graph.getVertex(i).y * yScale);
            graphPointsInGraph.add(new Point(x1, y1));
        }
        //create y grid
        g2.setColor(Color.lightGray);
        for (int i = 0; i <= size; i += yTick) {
            double x0 = BORDER_GAP;
            double x1 = getWidth() - BORDER_GAP;
            double y0 = getHeight() - BORDER_GAP - (yScale * i);
            double y1 = y0;
            line.setLine(x0, y0, x1, y1);
            g2.draw(line);
        }
        //create x grid
        for (int i = 0; i <= size; i += xTick) {
            double x0 = BORDER_GAP + (xScale * i);
            double x1 = x0;
            double y0 = BORDER_GAP;
            double y1 = getHeight() - BORDER_GAP;
            line.setLine(x0, y0, x1, y1);
            g2.draw(line);
        }
        Stroke oldStroke = g2.getStroke();
        g2.setStroke(new BasicStroke(1.8f));
        g2.setColor(Color.black);
        // create x and y axes 
        g2.drawLine(BORDER_GAP, getHeight() - BORDER_GAP, BORDER_GAP, BORDER_GAP);
        g2.drawLine(BORDER_GAP, BORDER_GAP, getWidth() - BORDER_GAP, BORDER_GAP);
        g2.drawLine(BORDER_GAP, getHeight() - BORDER_GAP, getWidth() - BORDER_GAP, getHeight() - BORDER_GAP);
        g2.drawLine(getWidth() - BORDER_GAP, BORDER_GAP, getWidth() - BORDER_GAP, getHeight() - BORDER_GAP);
        g2.setStroke(oldStroke);
        // create hatch marks for y axis. 
        for (int i = 0; i <= size; i += yTick) {
            double x0 = BORDER_GAP;
            double x1 = BORDER_GAP + GRAPH_POINT_WIDTH;
            double y0 = getHeight() - BORDER_GAP - (yScale * i);
            double y1 = y0;
            line.setLine(x0, y0, x1, y1);
            g2.draw(line);
            g2.setColor(Color.red);
            g2.drawString(Integer.toString(i), (float) x0 - (BORDER_GAP - 3), (float) y0 + 3);
            g2.setColor(Color.black);
            x0 = getWidth() - BORDER_GAP;
            x1 = x0 - GRAPH_POINT_WIDTH;
            line.setLine(x0, y0, x1, y1);
            g2.draw(line);
            if (i != 0) {
                g2.setColor(Color.red);
                g2.drawString(Integer.toString(i), (float) x0 + (BORDER_GAP / 4), (float) y0 + 3);
                g2.setColor(Color.black);
            }
        }
        // and for x axis
        for (int i = xTick; i <= size; i += xTick) {
            double x0 = BORDER_GAP + (xScale * i);
            double x1 = x0;
            double y0 = getHeight() - BORDER_GAP;
            double y1 = y0 - GRAPH_POINT_WIDTH;
            line.setLine(x0, y0, x1, y1);
            g2.draw(line);
            g2.setColor(Color.red);
            g2.drawString(Integer.toString(i), (float) x0 - 8, (float) y0 + BORDER_GAP - 10);
            g2.setColor(Color.black);
            y0 = BORDER_GAP;
            y1 = y0 + GRAPH_POINT_WIDTH;
            line.setLine(x0, y0, x1, y1);
            g2.draw(line);
            if (i != size) {
                g2.setColor(Color.red);
                g2.drawString(Integer.toString(i), (float) x0 - 8, (float) y0 - (BORDER_GAP / 4));
                g2.setColor(Color.black);
            }
        }

        //pinta o alcance
        Color c = new Color(200, 200, 0, 40);
        int pos = (int) (((sizeWave + sizeWave) * (int) xScale) * 1.07);
        if (!(route.length > 0)) {
            g2.setStroke(oldStroke);
            g2.setColor(c);
            for (int i = 0; i < graph.getNumberVertex(); i++) {
                
                int x = graphPointsInGraph.get(i).x - (pos / 2);
                int y = graphPointsInGraph.get(i).y - (pos / 2);
                int ovalW = pos;
                int ovalH = pos;
                g2.fillOval(x, y, ovalW, ovalH);
            }
        }
        //aqui cria a linha que liga os pontos
        oldStroke = g2.getStroke();
        g2.setColor(GRAPH_COLOR);
        g2.setStroke(GRAPH_STROKE);

        for (int i = 0; i < graph.getNumberVertex(); i++) {
            int x1 = graphPointsInGraph.get(i).x;
            int y1 = graphPointsInGraph.get(i).y;
            for (int j = 0; j < graph.getVertex(i).getNumberEdges(); j++) {
                int node = graph.getVertex(i).edge.get(j).destination.id;
                int x2 = graphPointsInGraph.get(node).x;
                int y2 = graphPointsInGraph.get(node).y;
                if (auxGraph.isEdge(graph.getVertex(i).edge.get(j).destination.id, graph.getVertex(i).id)) {
                    //TODO 
                    g2.setColor(new Color(216, 53, 65));
                    g2.setStroke(GRAPH_STROKE);
                    g2.drawLine(x1, y1, x2, y2);
                    auxGraph.addEdge(graph.getVertex(i).id, node, graph.getWeight(graph.getVertex(i).id, node));
                } else {
                    g2.setColor(new Color(73, 158, 119));
                    g2.setStroke(GRAPH_STROKE_SOURCE);
                    g2.drawLine(x1, y1, x2, y2);
                    auxGraph.addEdge(graph.getVertex(i).id, node, graph.getWeight(graph.getVertex(i).id, node));
                }

            }
        }

        //aqui que cria o ponto
        g2.setStroke(oldStroke);
        g2.setColor(GRAPH_POINT_COLOR);
        for (int i = 0; i < graphPointsInGraph.size(); i++) {
            if (isTree) {
                if (i == 0) {
                    g2.setColor(Color.yellow);
                } else {
                    g2.setColor(GRAPH_POINT_COLOR);
                }
            }
            int x = graphPointsInGraph.get(i).x - GRAPH_POINT_WIDTH / 2;
            int y = graphPointsInGraph.get(i).y - GRAPH_POINT_WIDTH / 2;
            int ovalW = GRAPH_POINT_WIDTH;
            int ovalH = GRAPH_POINT_WIDTH;
            g2.fillOval(x, y, ovalW, ovalH);
            if (showIds) {
                g2.setColor(Color.black);
                g2.drawString(Integer.toString(i), (float) x - 2, (float) y + 22);
                g2.setColor(GRAPH_POINT_COLOR);
            }
        }
        if (route.length > 0) {
            int i;
            
            for (i = 0; i < route.length - 1; i++) {
                int x0 = graphPointsInGraph.get(route[i]).x;
                int y0 = graphPointsInGraph.get(route[i]).y;
                int x1 = graphPointsInGraph.get(route[i + 1]).x;
                int y1 = graphPointsInGraph.get(route[i + 1]).y;
                int xP = x0 - (pos / 2);
                int yP = y0 - (pos / 2);
                int xS = x0 - (pos / 2);
                int yS = y0 - (pos / 2);
                int ovalW = pos;
                int ovalH = pos;
                g2.setColor(c);
                g2.fillOval(xP, yP, ovalW, ovalH);
                //g2.fillOval(xS, yS, ovalW, ovalH);
                g2.setColor(Color.blue);
                g2.setStroke(STROKE_ROUTE);
                g2.drawLine(x0, y0, x1, y1);
                g2.drawLine(x1, y1, x0, y0);
                if (i == 0) {
                    g2.setColor(Color.GREEN);
                    int x = x0 - GRAPH_POINT_WIDTH / 2;
                    int y = y0 - GRAPH_POINT_WIDTH / 2;
                    ovalW = GRAPH_POINT_WIDTH;
                    ovalH = GRAPH_POINT_WIDTH;
                    g2.fillOval(x, y, ovalW, ovalH);
                }else{
                    g2.setColor(GRAPH_POINT_COLOR);
                    int x = x0 - GRAPH_POINT_WIDTH / 2;
                    int y = y0 - GRAPH_POINT_WIDTH / 2;
                    ovalW = GRAPH_POINT_WIDTH;
                    ovalH = GRAPH_POINT_WIDTH;
                    g2.fillOval(x, y, ovalW, ovalH);
                }
            }
            int x0 = graphPointsInGraph.get(route[i]).x;
            int y0 = graphPointsInGraph.get(route[i]).y;
            g2.setColor(Color.red);
            int x = x0 - GRAPH_POINT_WIDTH / 2;
            int y = y0 - GRAPH_POINT_WIDTH / 2;
            int ovalW = GRAPH_POINT_WIDTH;
            int ovalH = GRAPH_POINT_WIDTH;
            g2.fillOval(x, y, ovalW, ovalH);
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(PREF_W, PREF_H);
    }

    @Override
    public String getName() {
        return name;
    }

    public static DrawGraph createAndShowGraph(Graph g, boolean showIds, int sizeWave, boolean isTree, boolean comp) {
        DrawGraph mainPanel;
        if(comp){
            mainPanel = new DrawGraph(g, g.getSize(), showIds, sizeWave, isTree, 380);
        }else{
            mainPanel = new DrawGraph(g, g.getSize(), showIds, sizeWave, isTree);
        }
        DrawGraph.name = "";
        return mainPanel;
    }

    public static DrawGraph createAndShowGraphForDijkstra(Graph g, boolean showIds, int sizeWave, int[] route, boolean isTree) {
        DrawGraph mainPanel = new DrawGraph(g, g.getSize(), showIds, route, sizeWave, isTree);
        DrawGraph.name = "";
        return mainPanel;
    }

    public static void createAndShowGraph(Graph g, String name, boolean showIds) {
        DrawGraph mainPanel = new DrawGraph(g, g.getSize(), showIds);
        DrawGraph.name = name;
        JFrame frame = new JFrame(name);
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(mainPanel);
        frame.pack();
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }

    public static void createAndShowGraphForDijkstra(Graph g, String name, boolean showIds, int[] route) {
        DrawGraph mainPanel = new DrawGraph(g, g.getSize(), showIds, route);
        DrawGraph.name = name;
        JFrame frame = new JFrame(name);
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(mainPanel);
        frame.pack();
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }

}
