/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.tools;

/**
 *
 * @author lucas
 */
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
 
public class SaveTextFile {
    
    BufferedWriter buffWrite;

    public SaveTextFile(String path) throws IOException {
        buffWrite = new BufferedWriter(new FileWriter(path));
    }
    public void print(String string) throws IOException {
        buffWrite.append(string);
    }
    public void close() throws IOException {
        buffWrite.close();
    }
}
