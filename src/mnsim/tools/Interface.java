/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.tools;

/**
 *
 * @author lucas
 */
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Interface  extends JFrame{

    public  Interface ()
    {
        //frame
        super("RMSimulator");

        //declaration container
        Container c;
        c=getContentPane();
        c.setLayout(new BorderLayout());
        c.setBackground(Color.white);
        //declaration des panel avec leurs caracteristiques

        JPanel menu =new JPanel(new GridLayout(0,1,3,3));
        JPanel messageList =new JPanel(new FlowLayout());
        JPanel about=new JPanel(new FlowLayout());

        menu.setBackground(Color.blue);
        messageList.setBackground(Color.cyan);
        messageList.add(new JLabel("'messageList' padder"));
        about.setBackground(Color.green);
        about.add(new JLabel("'about' padder"));

        JPanel menuConstrain = new JPanel(new BorderLayout());
        menuConstrain.setBackground(Color.yellow);

        menuConstrain.add(menu,BorderLayout.NORTH);
        c.add(menuConstrain,BorderLayout.WEST);
        c.add(messageList,BorderLayout.EAST);
        c.add(about,BorderLayout.SOUTH);
        //--------Button---------------------
        JButton button1=new JButton("button1");
        JButton button2=new JButton("Button2");

        menu.add(button1);
        menu.add(button2);
        
        button1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                messageList.setBackground(Color.pink);
            }
        });
        //-----------------------------

        pack();
        setSize(300,150);
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
