/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;

/**
 *
 * @author lucas
 */
public abstract class Event {
    
    private double time;
   
    /**
     * Sets a new time for the Event to happen.
     * 
     * @param time new scheduled period
     */
    public void setTime(double time){
        this.time = time;
    }
    
    /**
     * Retrieves current scheduled time for a given Event. 
     * 
     * @return value of the Event's time attribute
     */
    public double getTime() {
        return this.time;
    }
}
