/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.mac;

import mnsim.EventScheduler;
import mnsim.Packet;
import mnsim.Path;
import mnsim.Topology;

/**
 *
 * @author lucas
 */
public interface MAC {
    
    public void simulationInterface(Topology top, EventScheduler events);
    
    public void sendingAction(Packet pack, Path path, double currentEventTime);
    
    public void departurePacket(Packet pack, Path path);
    
    public void collisionAction(int nodes[]);
    
}

