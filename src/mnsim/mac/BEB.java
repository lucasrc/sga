/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.mac;

import java.util.Random;
import java.util.TreeSet;
import mnsim.Defines;
import mnsim.Event;
import mnsim.EventScheduler;
import mnsim.Packet;
import mnsim.PacketArrivalEvent;
import mnsim.PacketDepartureEvent;
import mnsim.Path;
import mnsim.Topology;

/**
 *
 * @author lucas
 */
public class BEB implements MAC {
    
    private static final int CWMIN = 32;
    private static final int CWMAX = 1024;

    Random generator = new Random();
    private int[] backoff;
    private int[] cw;
    
    private Topology top;
    private EventScheduler events;
    
    private TreeSet<Long> dataOccurrence;

    
    @Override
    public void simulationInterface(Topology top, EventScheduler events) {
        this.top = top;
        this.events = events;
        this.cw = new int[top.getNumNodes()];
        this.backoff = new int[top.getNumNodes()];
        for (int i = 0; i < backoff.length; i++) {
            cw[i] = CWMIN;
            backoff[i] = generator.nextInt(cw[i]);
        }
        this.dataOccurrence = new TreeSet<>();
    }

    @Override
    public void sendingAction(Packet pack, Path path, double currentEventTime) {
        if (pack.getType() == Defines.PacketDataType) {
            dataAction(pack, path, currentEventTime);
        } else {
            if (pack.getType() == Defines.PacketRTSType) {
                rtsAction(pack, path, currentEventTime);
            } else {
                if (pack.getType() == Defines.PacketCTSType) {
                    ctsAction(pack, path, currentEventTime);
                } else {
                    if (pack.getType() == Defines.PacketACKType) {
                        ackAction(pack, path, currentEventTime);
                    } else {
                        //TODO
                        noiseAction(pack, path, currentEventTime);
                    }
                }
            }
        }
    }


    private void dataAction(Packet data, Path path, double currentEventTime) {
        if (!dataOccurrence.contains(data.getId())) {
            dataOccurrence.add(data.getId());
            int src = data.getSource();
            int dst = data.getDestination();
            top.getNode(src).setIdleState();
            backoff[src] = generator.nextInt(cw[src]);
            Event event = new PacketArrivalEvent(new Packet(data.getId(), src, dst, Defines.PacketRTSType));
            event.setTime(currentEventTime + backoff[src]);
            events.addEvent(event);
        } else {
            int src = data.getSource();
            int dst = data.getDestination();
            top.getNode(src).setSendingState();//mandando o data
            //each node around of dst (cts's src) set nav state the cts packet
            int neighbors[] = top.getNeighborsGraph().neighbors(dst);
            for (int i = 0; i < neighbors.length; i++) {
                top.getNode(neighbors[i]).setNAVState();
            }
            top.getNode(dst).setListeningState();//ouvindo o data
            Event event = new PacketArrivalEvent(new Packet(data.getId(), src, dst, Defines.PacketACKType));
            event.setTime(currentEventTime + Defines.SIFS_TIME + Defines.DATA_TIME);
            events.addEvent(event);
        }
    }

    private void ackAction(Packet ack, Path path, double currentEventTime) {
        int src = ack.getSource();
        int dst = ack.getDestination();
        top.getNode(src).setListeningState();//esperando o ack
        top.getNode(dst).setSendingState();//mandando o ack
        Event event = new PacketDepartureEvent(ack.getId());
        event.setTime(currentEventTime + Defines.SIFS_TIME + Defines.ACK_TIME + Defines.DIFS_TIME);
        events.addEvent(event);
    }

    private void ctsAction(Packet cts, Path path, double currentEventTime) {
        int src = cts.getSource();
        int dst = cts.getDestination();
        top.getNode(src).setListeningState();//esperando o rts
        //each node around of src set nav state the rts packet
        int neighbors[] = top.getNeighborsGraph().neighbors(src);
        for (int i = 0; i < neighbors.length; i++) {
            top.getNode(neighbors[i]).setNAVState();
        }
        top.getNode(dst).setSendingState();//mandando o cts
        Event event = new PacketArrivalEvent(new Packet(cts.getId(), src, dst, Defines.PacketDataType));
        event.setTime(currentEventTime + Defines.SIFS_TIME + Defines.CTS_TIME);
        events.addEvent(event);
    }

    private void rtsAction(Packet rts, Path path, double currentEventTime) {
        int src = rts.getSource();
        int dst = rts.getDestination();
        top.getNode(src).setSendingState();//mandando rts
        //each node around of src listening the rts packet
        int neighbors[] = top.getNeighborsGraph().neighbors(src);
        for (int i = 0; i < neighbors.length; i++) {
            top.getNode(neighbors[i]).setListeningState();//todos estao ouvindo o rts
        }
        Event event = new PacketArrivalEvent(new Packet(rts.getId(), src, dst, Defines.PacketCTSType));
        event.setTime(currentEventTime + Defines.RTS_TIME);
        events.addEvent(event);
    }
    
    private void noiseAction(Packet noise, Path path, double currentEventTime) {
        
    }

    @Override
    public void departurePacket(Packet pack, Path path) {
        int src = pack.getSource();
        int dst = pack.getDestination();
        top.getNode(src).setIdleState();
        top.getNode(dst).setIdleState();
        
        int neighbors[] = top.getNeighborsGraph().neighbors(src);
        for (int i = 0; i < neighbors.length; i++) {
            top.getNode(neighbors[i]).setIdleState();
        }
        
        neighbors = top.getNeighborsGraph().neighbors(dst);
        for (int i = 0; i < neighbors.length; i++) {
            top.getNode(neighbors[i]).setIdleState();
        }
    }
    
    @Override
    public void collisionAction(int nodes[]) {
        for (int i = 0; i < nodes.length; i++) {
            if(cw[nodes[i]] < CWMAX){
                cw[nodes[i]] = cw[nodes[i]]*2;
            }
            backoff[nodes[i]] = generator.nextInt(cw[nodes[i]]);
        }
    }
    
}
