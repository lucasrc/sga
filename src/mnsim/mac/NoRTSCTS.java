/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.mac;

import java.util.Random;
import java.util.TreeSet;
import mnsim.Defines;
import mnsim.Event;
import mnsim.EventScheduler;
import mnsim.Packet;
import mnsim.PacketArrivalEvent;
import mnsim.PacketDepartureEvent;
import mnsim.Path;
import mnsim.Topology;

/**
 *
 * @author lucas
 */
public class NoRTSCTS implements MAC {
    
    private static final int CWMIN = 32;
    private static final int CWMAX = 1024;

    Random generator = new Random();
    private int[] backoff;
    private int[] cw;
    
    private Topology top;
    private EventScheduler events;
    
    private TreeSet<Long> dataOccurrence;

    
    @Override
    public void simulationInterface(Topology top, EventScheduler events) {
        this.top = top;
        this.events = events;
        this.cw = new int[top.getNumNodes()];
        this.backoff = new int[top.getNumNodes()];
        for (int i = 0; i < backoff.length; i++) {
            cw[i] = CWMIN;
            backoff[i] = generator.nextInt(cw[i]);
        }
        this.dataOccurrence = new TreeSet<>();
    }

    @Override
    public void sendingAction(Packet pack, Path path, double currentEventTime) {
        if(pack.getType() == Defines.PacketDataType){
            dataAction(pack, path, currentEventTime);
        } else {
            if(pack.getType() == Defines.PacketACKType){
                ackAction(pack, path, currentEventTime);
            } else {
                //TODO
                noiseAction(pack, path, currentEventTime);
            }
        }
    }

    private void dataAction(Packet data, Path path, double currentEventTime) {
        if (!dataOccurrence.contains(data.getId())) {
            dataOccurrence.add(data.getId());
            int src = data.getSource();
            int dst = data.getDestination();
            top.getNode(src).setIdleState();
            backoff[src] = generator.nextInt(cw[src]);
            Event event = new PacketArrivalEvent(new Packet(data.getId(), src, dst, Defines.PacketDataType));
            event.setTime(currentEventTime + backoff[src]);
            events.addEvent(event);
        } else {
            int src = data.getSource();
            int dst = data.getDestination();
            for (int i = 0; i < path.getPath().length - 1; i++) {//o src ta mandando
                top.getNode(path.getPath()[i]).setSendingState();//mandando o data
            }
            top.getNode(dst).setListeningState();//ouvindo o data
            Event event = new PacketArrivalEvent(new Packet(data.getId(), src, dst, Defines.PacketACKType));
            event.setTime(currentEventTime + Defines.SIFS_TIME + Defines.DATA_TIME);
            events.addEvent(event);
        }
    }

    private void ackAction(Packet ack, Path path, double currentEventTime) {
        int src = ack.getSource();
        int dst = ack.getDestination();
        top.getNode(src).setListeningState();//esperando o ack
        for (int i = 1; i < path.getPath().length; i++) { //o dst ta mandando
            top.getNode(path.getPath()[i]).setSendingState();//mandando o ack
        }
        Event event = new PacketDepartureEvent(ack.getId());
        event.setTime(currentEventTime + Defines.SIFS_TIME + Defines.ACK_TIME + Defines.DIFS_TIME);
        events.addEvent(event);
    }
    
    @Override
    public void departurePacket(Packet pack, Path path) {
        for (int i = 0; i < path.getPath().length; i++) { //todos da rota
            top.getNode(path.getPath()[i]).setIdleState();
        }
    }
    
    private void noiseAction(Packet noise, Path path, double currentEventTime) {
        
    }
    
    @Override
    public void collisionAction(int nodes[]) {
        for (int i = 0; i < nodes.length; i++) {
            if(cw[nodes[i]] < CWMAX){
                cw[nodes[i]] = cw[nodes[i]]*2;
            }
            backoff[nodes[i]] = generator.nextInt(cw[nodes[i]]);
        }
    }
    
}
