/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;

/**
 * The Main class takes care of the execution of the simulator,
 * which includes dealing with the arguments called (or not) on
 * the command line.
 * 
 * @author lucasrc
 */
public class Main {

    /**
     * Instantiates a Simulator object and takes the arguments from the command line.
     * Based on the number of arguments, can detect if there are too many or too few,
     * which prints a message teaching how to run MNSim. If the number is correct,
     * detects which arguments were applied and makes sure they have the expected effect.
     * 
     * @param args the command line arguments
     */
    public static void main2(String[] args) {
        Simulator mnsim;
        String usage = "Usage: MNSim simulation_file seedTraffic seedGraph [-trace] [-verbose]";
        String simConfigFile;
        boolean verbose = false;
        boolean trace = false;
        int seedTraffic, seedGraph;
        double minload = 0, maxload = 0, step = 1;

        if (args.length < 3 || args.length > 5) {
            System.out.println(usage);
            System.exit(0);
        } else {
            if (args.length == 4) {
                if (args[3].equals("-verbose")) {
                    verbose = true;
                } else {
                    if (args[3].equals("-trace")) {
                        trace = true;
                    } else {
                        System.out.println(usage);
                        System.exit(0);
                    }
                }
            }
            if (args.length == 5) {
                if ((args[3].equals("-trace") && args[4].equals("-verbose")) || (args[4].equals("-trace") && args[3].equals("-verbose"))) {
                    trace = true;
                    verbose = true;
                } else {
                    System.out.println(usage);
                    System.exit(0);
                }
            }
        }

        simConfigFile = args[0];
        seedTraffic = Integer.parseInt(args[1]);
        seedGraph = Integer.parseInt(args[2]);
        mnsim = new Simulator();
        mnsim.Execute(simConfigFile, trace, verbose, seedTraffic, seedGraph);
    }
}
