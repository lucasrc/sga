/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim;

/**
 *
 * @author lucas
 */
public class SimulationRunner {

    SimulationRunner(ControlMedium cp, EventScheduler events) {
        Event event;
        Tracer tr = Tracer.getTracerObject();
        MyStatistics st = MyStatistics.getMyStatisticsObject();
        while ((event = events.popEvent()) != null) {
            tr.add(event);
            st.addEvent(event);
            cp.newEvent(event);
        }
    }
    
}
