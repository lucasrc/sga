/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.ra;

import mnsim.ControlMedium;
import mnsim.Packet;

/**
 *
 * @author lucas
 */
public interface RA {
    
    public void simulationInterface(ControlMedium cp);
    
    public void packetArrival(Packet pack);
    
    public void packetDeparture(long id);
    
}
