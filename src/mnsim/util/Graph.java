/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnsim.util;

/**
 *
 * @author lucas
 */
import java.util.List;
import java.util.ArrayList;
import java.util.TreeSet;

public class Graph {
    
    public class Vertex {
        public int id;
        public int x;
        public int y;
        public List<Edge> edge;

        Vertex(int id, int x, int y) {
            this.id = id;
            this.x = x;
            this.y = y;
            this.edge = new ArrayList<Edge>();
        }
        
        void addEdge(Vertex destination, double weight) {
            edge.add(new Edge(this, destination, weight));
        }
        
        boolean removeEdge(int destination) {
            for (Edge e : edge) {
                if(e.destination.id == destination){
                    return edge.remove(e);
                }
            }
            return false;
        }
        
        public Vertex[] neighbors(){
            Vertex neighbors[] = new Vertex[edge.size()];
            for (int i = 0; i < neighbors.length; i++) {
                neighbors[i] = edge.get(i).destination;
            }
            return neighbors;
        }
        
        public int getNumberEdges(){
            return edge.size();
        }
    }

    public class Edge {
        public Vertex source;
        public Vertex destination;
        public double weight;

        Edge(Vertex source, Vertex destination, double weight) {
            this.source = source;
            this.destination = destination;
            this.weight = weight;
        }
    }

    private int size;
    private List<Vertex> vertex;

    public Graph(int size) {
        this.size = size;
        vertex = new ArrayList<>();
    }
    
    public Graph(Graph g) {
        this.size = g.size;
        vertex = new ArrayList<>();
        for (Vertex newVertex : g.vertex) {
            vertex.add(new Vertex(newVertex.id, newVertex.x, newVertex.y));
        }
        for (int i = 0; i < g.vertex.size(); i++) {
            for (int j = 0; j < g.vertex.get(i).getNumberEdges(); j++) {
                vertex.get(i).addEdge(vertex.get(g.vertex.get(i).edge.get(j).destination.id), g.vertex.get(i).edge.get(j).weight);
            }
        }
    }
    
    public Graph(Graph g, boolean onlynodes) {
        if (onlynodes) {
            this.size = g.size;
            vertex = new ArrayList<>();
            for (Vertex newVertex : g.vertex) {
                vertex.add(new Vertex(newVertex.id, newVertex.x, newVertex.y));
            }
        } else {
            this.size = g.size;
            vertex = new ArrayList<>();
            for (Vertex newVertex : g.vertex) {
                vertex.add(new Vertex(newVertex.id, newVertex.x, newVertex.y));
            }
            for (int i = 0; i < g.vertex.size(); i++) {
                for (int j = 0; j < g.vertex.get(i).getNumberEdges(); j++) {
                    vertex.get(i).addEdge(vertex.get(g.vertex.get(i).edge.get(j).destination.id), g.vertex.get(i).edge.get(j).weight);
                }
            }
        }
    }

    public void addVertex(int id, int x, int y) {
        if(x > this.size || y > this.size || x < 0 || y < 0){
            throw (new IllegalArgumentException());
        }
        Vertex v = new Vertex(id, x, y);
        vertex.add(v);
    }
    
    public boolean isEdge(int source, int destination) {
        for (int i = 0; i < getVertex(source).getNumberEdges(); i++) {
            if(getVertex(source).edge.get(i).destination.id == destination){
                return true;
            }
        }
        return false;
    }
    
    public boolean isVertex(int x, int y) {
        for (Vertex v : vertex) {
            if(v.x == x && v.y == y){
                return true;
            }
        }
        return false;
    }
    
    public void addEdge(int source, int destination, double weight) {
        if(!isEdge(source, destination)){
            getVertex(source).addEdge(getVertex(destination), weight);
        }
    }
    
    public void removeEdge(int source, int destination) {
        getVertex(source).removeEdge(destination);
    }
    
    public double getWeight(int source, int destination){
        for (int i = 0; i < getVertex(source).getNumberEdges(); i++) {
            if(getVertex(source).edge.get(i).destination.id == destination){
                return getVertex(source).edge.get(i).weight;
            }
        }
        return -1;
    }

    public int getSize() {
        return size;
    }
    
    public int getNumberVertex() {
        return vertex.size();
    }

    public List<Vertex> getVertex() {
        return vertex;
    }
    
    public Vertex getVertex(int id) {
        for (Vertex v : vertex) {
            if(v.id == id){
                return v;
            }
        }
        return null;
    }
    
    public Edge getEdge(int source, int destination) {
        for (Edge edge : getVertex(source).edge) {
            if(edge.destination.id == destination){
                return edge;
            }
        }
        return null;
    }
    
    /**
     * Retrieves the neighbors of a given vertex. 
     * 
     * @param node index of the vertex within the matrix of edges
     * @return list with indexes of the vertex's neighbors
     */
    public int[] neighbors(int node) {
        int neighbors[] = new int[getVertex(node).edge.size()];
        for (int i = 0; i < neighbors.length; i++) {
            neighbors[i] = getVertex(node).edge.get(i).destination.id;
        }
        return neighbors;
    }
    
    /**
     * Remode all edges node
     * 
     * @param node the node
     */
    public void removeVertexEdges(int node) {
        //removing edges from this node
        for (Vertex v : vertex) {
            if(isEdge(node, v.id)){
                removeEdge(node, v.id);
            }
            if(isEdge(v.id, node)){
                removeEdge(v.id, node);
            }
        }
    }
    
    public void removeVertex(int node) {
        //removing edges from this node
        removeVertexEdges(node);
        //remove node from the graph
        ArrayList<Vertex> newVertex = new ArrayList<>(vertex);
        for (Vertex v : vertex) {
            if(v.id == node){
                newVertex.remove(v);
            }
        }
        vertex = newVertex;
    }
    
    /**
     * Prints all information related to the weighted graph.
     * For each vertex, shows the vertexes is is adjacent to and the
     * weight of each edge.
     * 
     * @return string containing the edges of each vertex
     */
    @Override
    public String toString() {
        String r = "";
        for (Vertex u : vertex) {
            r += u.id + "[" + u.x + "," + u.y + "]" + " -> ";
            for (Edge e : u.edge) {
                Vertex v = e.destination;
                r += v.id + ", ";//["+u.edge.get(v.id).weight+"], ";
            }
            r += "\n";
        }
        return r;
    }
    
    /**
     * Retrieves the min Route of this graph.
     * @return the least of all the calculated shortest paths in a network
     */
    public double getMinRoute(){
        double minRoute = Double.MAX_VALUE, routeLength = 0;
        int[] nodes;
        for (int i = 0; i < getNumberVertex(); i++) {
            for (int j = 0; j < getNumberVertex(); j++) {
                if (i != j) {
                    nodes = Dijkstra.getShortestPath(this, i, j);
                    if (nodes.length != 0) {
                        for (int k = 0; k < nodes.length - 1; k++) {
                            routeLength += getWeight(nodes[k], nodes[k + 1]);
                        }
                        if (routeLength < minRoute) {
                            minRoute = routeLength;
                        }
                    }
                }
                routeLength = 0;
            }
        }
        return minRoute;
    }
    
    /**
     * Retrieves the diameter of this graph.
     * @return the longest of all the calculated shortest paths in a network
     */
    public double getGraphDiameter(){
        double maxRoute = 0, routeLength = 0;
        int[] nodes;
        for (int i = 0; i < getNumberVertex(); i++) {
            for (int j = 0; j < getNumberVertex(); j++) {
                if (i != j) {
                    nodes = Dijkstra.getShortestPath(this, i, j);
                    for (int k = 0; k < nodes.length - 1; k++) {
                        routeLength += getWeight(nodes[k], nodes[k + 1]);
                    }
                    if(routeLength > maxRoute){
                        maxRoute = routeLength;
                    }
                }
                routeLength = 0;
            }
        }
        return maxRoute;
    }
    
    /**
     * Retrieves the clustering coefficient of this node.
     * The clustering coefficient of a node is the ratio of existing links connecting a node's neighbors to each other to the maximum possible number of such links.
     * 
     * @param node the node
     * @return the clustering coefficient
     */
    public double getClusteringCoefficient(int node){
        int[] neighbors = neighbors(node);
        int connections = 0;
        for (int i = 0; i < neighbors.length; i++) {
            for (int j = 0; j < neighbors.length; j++) {
                if(neighbors[i] != neighbors[j]){
                    if(isEdge(neighbors[i], neighbors[j])){
                        connections++;
                    }
                }
            }
            
        }
        return (double) connections/ ((double) neighbors.length * ( (double) neighbors.length - 1.0));
    }
    
    /**
     * Retrieves the max local clustering coefficient in this graph.
     * 
     * @return the max local clustering coefficient
     */
    public double getMaxClusteringCoefficient(){
        double max = 0;
        double aux;
        for (int i = 0; i < getNumberVertex(); i++) {
            aux = getClusteringCoefficient(i);
            if(aux > max){
                max = aux;
            }
        }
        return max;
    }
    
    /**
     * Retrieves the sum of all local clustering coefficient in this graph.
     * 
     * @return the sum of all local clustering coefficient
     */
    public double getSumClusteringCoefficient(){
        double sum = 0;
        for (int i = 0; i < getNumberVertex(); i++) {
            sum += getClusteringCoefficient(i);
        }
        return sum;
    }
    
    /**
     * Retrieves the average clustering coefficient of this graph.
     * 
     * @return the average clustering coefficient
     */
    public double getAverageClusteringCoefficient(){
        return getSumClusteringCoefficient() / (double) getNumberVertex();
    }
    
    /**
     * Retrieves the global clustering coefficient of this graph.
     * 
     * @return the global clustering coefficient
     */
    public double getGlobalClusteringCoefficient(){
        ArrayList<TreeSet<Integer>> triangles = new ArrayList<>();
        ArrayList<TreeSet<Integer>> triplets = new ArrayList<>();
        TreeSet<Integer> triangle;
        TreeSet<Integer> triplet;
        for (int node = 0; node < getNumberVertex(); node++) {
            int[] neighbors = neighbors(node);
            for (int i = 0; i < neighbors.length; i++) {
                for (int j = 0; j < neighbors.length; j++) {
                    if (neighbors[i] != neighbors[j]) {
                        triplet = new TreeSet<>();
                        triplet.add(node);
                        triplet.add(neighbors[i]);
                        triplet.add(neighbors[j]);
                        if (!triplets.contains(triplet)) {
                            triplets.add(triplet);
                        }
                        if (isEdge(neighbors[i], neighbors[j])) {      
                            triangle = new TreeSet<>();
                            triangle.add(node);
                            triangle.add(neighbors[i]);
                            triangle.add(neighbors[j]);
                            if(!triangles.contains(triangle)){
                                triangles.add(triangle);
                            }
                        }
                    }
                }

            }
        }
        return (double) triangles.size() / (double) triplets.size();
    }
    
    /**
     * Retrieves the Average path length of this graph.
     * Average path length is calculated by finding the shortest path between all pairs of nodes, adding them up, and then dividing by the total number of pairs.
     * @return the average path length of this graph
     */
    public double getAveragePathLength(){
        double sumDistance = 0;
        int[] nodes;
        int nodePairs = 0;
        for (int i = 0; i < getNumberVertex(); i++) {
            for (int j = 0; j < getNumberVertex(); j++) {
                if(i != j){
                    nodes = Dijkstra.getShortestPath(this, i, j);
                    if(nodes.length != 0){
                        nodePairs++;
                    }
                    for (int k = 0; k < nodes.length - 1; k++) {
                        sumDistance += getWeight(nodes[k], nodes[k + 1]);
                    }
                }
            }
        }
        return sumDistance/ (double) nodePairs;
    }
    
    public double getCost(int radius) {
        double cost = 0;
        double maxEdge;
        for (int i = 0; i < getNumberVertex(); i++) {
            maxEdge = 0;
            for (int j = 0; j < vertex.get(i).edge.size(); j++) {
                if(vertex.get(i).edge.get(j).weight > maxEdge){
                    maxEdge = vertex.get(i).edge.get(j).weight;
                }
            }
            cost += Math.pow(maxEdge, 2);
        }
        //return (cost*100)/(radius*getNumberVertex());
        return cost;
    }
}
