# SGA - Simulador de Grafos e Árvores para controles de topologia

O SGA tem por objetivo a visualização de controle de topologia e grafos foi desenvolvido com o intuito de facilitar o aprendizado sobre controles de topologias, grafos e árvores, por se tratarem de conteúdos teóricos, muitos alunos tem dificuldade em realizar a representação das mesmas, pretende-se aqui ajudar tanto o professor, quanto o aluno, a representar e fixar o referente conteúdo. 

# Desenvolvido por: 

    Kaio Alexandre da Silva - k4iodm@gmail.com 
    Lucas Rodrigues Costa - lucasrc.rodri@gmail.com 